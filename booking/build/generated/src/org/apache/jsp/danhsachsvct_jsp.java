package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Model.SinhVienModel;
import java.util.ArrayList;
import DAO.Impl.SinhVienDAO;

public final class danhsachsvct_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(3);
    _jspx_dependants.add("/header.jsp");
    _jspx_dependants.add("/sidebar.jsp");
    _jspx_dependants.add("/footer.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">\n");
      out.write("        <title>Quản lí thi</title>\n");
      out.write("\n");
      out.write("        <!-- Custom fonts for this template-->\n");
      out.write("        <link href=\"vendor/fontawesome-free/css/all.min.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <link href=\"https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("        <!-- Custom styles for this template-->\n");
      out.write("        <link href=\"css/sb-admin-2.min.css\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("</html>");
      out.write("\n");
      out.write("\n");
      out.write("    <body id=\"page-top\">\n");
      out.write("\n");
      out.write("        <!-- Page Wrapper -->\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("\n");
      out.write("            <!-- Sidebar -->\n");
      out.write("            ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <body>\n");
      out.write("        <!-- Sidebar -->\n");
      out.write("        <ul class=\"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion\" id=\"accordionSidebar\">\n");
      out.write("\n");
      out.write("            <!-- Sidebar - Brand -->\n");
      out.write("            <a class=\"sidebar-brand d-flex align-items-center justify-content-center\" href=\"admin-home.jsp\">\n");
      out.write("                <div class=\"sidebar-brand-icon rotate-n-15\">\n");
      out.write("                    <i class=\"fas fa-laugh-wink\"></i>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"sidebar-brand-text mx-3\">ExamReg Admin</div>\n");
      out.write("            </a>\n");
      out.write("\n");
      out.write("            <!-- Divider -->\n");
      out.write("            <hr class=\"sidebar-divider my-0\">\n");
      out.write("\n");
      out.write("            <!-- Nav Item - Dashboard -->\n");
      out.write("            <li class=\"nav-item active\">\n");
      out.write("                <a class=\"nav-link\" href=\"admin-home.jsp\">\n");
      out.write("                    <i class=\"fas fa-fw fa-tachometer-alt\"></i>\n");
      out.write("                    <span>Dashboard</span>\n");
      out.write("                </a>\n");
      out.write("            </li>\n");
      out.write("\n");
      out.write("            <!-- Divider -->\n");
      out.write("            <hr class=\"sidebar-divider\">\n");
      out.write("            \n");
      out.write("            <!-- Quan ly mon thi -->\n");
      out.write("            <li class=\"nav-item\">\n");
      out.write("                <a class=\"nav-link collapsed\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapseTwo\" aria-expanded=\"true\" aria-controls=\"collapseTwo\">\n");
      out.write("                    <span>Quản lý tài khoản</span>\n");
      out.write("                </a>\n");
      out.write("                <div id=\"collapseTwo\" class=\"collapse\" aria-labelledby=\"headingTwo\" data-parent=\"#accordionSidebar\">\n");
      out.write("                    <div class=\"bg-white py-2 collapse-inner rounded\">\n");
      out.write("                        <a class=\"collapse-item\" href=\"login\">Đăng xuất</a>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </li>\n");
      out.write("            \n");
      out.write("            <!-- Divider -->\n");
      out.write("            <hr class=\"sidebar-divider\">\n");
      out.write("\n");
      out.write("            <!-- Quan ly sinh vien -->\n");
      out.write("            <li class=\"nav-item\">\n");
      out.write("                <a class=\"nav-link collapsed\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapseUtilities\" aria-expanded=\"true\" aria-controls=\"collapseUtilities\">\n");
      out.write("                    <span>Quản lý sinh viên</span>\n");
      out.write("                </a>\n");
      out.write("                <div id=\"collapseUtilities\" class=\"collapse\" aria-labelledby=\"headingUtilities\" data-parent=\"#accordionSidebar\">\n");
      out.write("                    <div class=\"bg-white py-2 collapse-inner rounded\">\n");
      out.write("                        <!-- Quan ly thông tin sinh viên -->\n");
      out.write("                        <h6 class=\"collapse-header\">Thông tin sinh viên:</h6>\n");
      out.write("                        <a class=\"collapse-item\" href=\"DanhSachSinhVien\">Danh sách sinh viên</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"themdssv\">Thêm danh sach sinh viên</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"themsinhvien.jsp\">Thêm sinh viên</a>\n");
      out.write("                        <!-- Quan ly điều kiện thi -->\n");
      out.write("                        <div class=\"collapse-divider\"></div>\n");
      out.write("                        <h6 class=\"collapse-header\">Thông tin thi:</h6>\n");
      out.write("                        <a class=\"collapse-item\" href=\"DanhSachThi\">Danh sách dự thi</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"DanhSachCamThi\">Danh sách cấm thi</a>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </li>\n");
      out.write("            \n");
      out.write("            <!-- Divider -->\n");
      out.write("            <hr class=\"sidebar-divider\">\n");
      out.write("\n");
      out.write("            <!-- Quan ly công tác thi -->\n");
      out.write("            <li class=\"nav-item\">\n");
      out.write("                <a class=\"nav-link collapsed\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapsePages\" aria-expanded=\"true\" aria-controls=\"collapsePages\">\n");
      out.write("                    <span>Quản lý kì thi</span>\n");
      out.write("                </a>\n");
      out.write("                <div id=\"collapsePages\" class=\"collapse\" aria-labelledby=\"headingPages\" data-parent=\"#accordionSidebar\">\n");
      out.write("                    <div class=\"bg-white py-2 collapse-inner rounded\">\n");
      out.write("                        <!-- Quan ly mon thi -->\n");
      out.write("                        <h6 class=\"collapse-header\">Quản lý môn thi:</h6>\n");
      out.write("                        <a class=\"collapse-item\" href=\"themmonhoc.jsp\">Thêm môn thi</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"TimKiemMaMh\">Danh sách môn thi</a>\n");
      out.write("                        <!-- Quan ly ky thi -->\n");
      out.write("                        <h6 class=\"collapse-header\">Quản lý kì thi:</h6>\n");
      out.write("                        <a class=\"collapse-item\" href=\"taokithi\">Tạo kì thi mới</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"DanhSachKiThi\">Danh sách các kì thi</a>\n");
      out.write("                        <!-- Quan ly ca thi -->\n");
      out.write("                        <div class=\"collapse-divider\"></div>\n");
      out.write("                        <h6 class=\"collapse-header\">Quản lý ca thi:</h6>\n");
      out.write("                        <a class=\"collapse-item\" href=\"themcathi\">Tạo ca thi mới</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"DanhSachCaThi\">Danh sách các ca thi</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"DanhSachSVCT\">Danh sách thi</a>\n");
      out.write("                        <!-- Quan ly phong thi -->\n");
      out.write("                        <div class=\"collapse-divider\"></div>\n");
      out.write("                        <h6 class=\"collapse-header\">Quản lý phòng thi:</h6>\n");
      out.write("                        <a class=\"collapse-item\" href=\"TaoPhongThi\">Tạo phòng thi mới</a>\n");
      out.write("                        <a class=\"collapse-item\" href=\"DanhSachPhongThi\">Danh sách các phòng thi</a>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </li>\n");
      out.write("            \n");
      out.write("            \n");
      out.write("            <!-- Divider -->\n");
      out.write("            <hr class=\"sidebar-divider d-none d-md-block\">\n");
      out.write("            <!-- Sidebar Toggler (Sidebar) -->\n");
      out.write("            <div class=\"text-center d-none d-md-inline\">\n");
      out.write("              <button class=\"rounded-circle border-0\" id=\"sidebarToggle\"></button>\n");
      out.write("            </div>\n");
      out.write("        </ul>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("            <!-- End of Sidebar -->\n");
      out.write("\n");
      out.write("            <!-- Content Wrapper -->\n");
      out.write("            <!-- Content Wrapper -->\n");
      out.write("            <div id=\"content-wrapper\" class=\"d-flex flex-column\">\n");
      out.write("                <div class=\"p-5\" style=\"height: 200px\">\n");
      out.write("                    <div class=\"text-center\">\n");
      out.write("                        <h1 class=\"h4 text-gray-900 mb-4\"><font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">Danh sách sinh viên</font></font></h1>\n");
      out.write("                    </div>\n");
      out.write("                    <form action=\"DanhSachSVCT\" method=\"post\">\n");
      out.write("                        <div class=\"form-group row\">\n");
      out.write("                            <div class=\"col-sm-6 mb-3 mb-sm-0\">\n");
      out.write("                                <h6>Nhập mã ca thi:</h6>\n");
      out.write("                                <input type=\"text\" class=\"form-control form-control-user\" name=\"smact\" value=\"");
      out.print(((String) request.getAttribute("idct") == null) ? "" : (String) request.getAttribute("idct"));
      out.write("\">\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"col-sm-6\">\n");
      out.write("                                <button type=\"submit\" id=\"timmonhoc\" class=\"btn btn-primary btn-user btn-block\" style=\"width: 60px;margin-left: 450px\" >Tìm</button>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </form>\n");
      out.write("                </div>\n");
      out.write("                <!-- Main Content -->\n");
      out.write("                <div id=\"content\">\n");
      out.write("                    <!-- Begin Page Content -->\n");
      out.write("                    <div class=\"container-fluid\">\n");
      out.write("                        <!-- DataTales Example -->\n");
      out.write("                        <div class=\"card shadow mb-4\">\n");
      out.write("                            <div class=\"card-body\">\n");
      out.write("                                <div class=\"table-responsive\">\n");
      out.write("                                    <table class=\"table table-bordered\" id=\"dataTable\"  cellspacing=\"0\">\n");
      out.write("                                        <thead>\n");
      out.write("                                            <tr>\n");
      out.write("                                                <th>Mã sinh viên</th>\n");
      out.write("                                                <th>Họ tên</th>\n");
      out.write("                                                <th>Ngày sinh</th>\n");
      out.write("                                                <th>Lớp</th>\n");
      out.write("                                                <th>Ghi chú</th>\n");
      out.write("                                            </tr>\n");
      out.write("                                        </thead>\n");
      out.write("                                        ");

                                            out.print("<tbody>");
                                            String id = (String) request.getAttribute("idct");

                                            ArrayList<SinhVienModel> list = (ArrayList<SinhVienModel>) request.getAttribute("SVCT");

                                            int i = 0;
                                            while (i < list.size()) {
                                                SinhVienModel sv = list.get(i);
                                                out.print("<tr>");
                                                out.print("<td>" + sv.getMasv() + "</td>");
                                                out.print("<td>" + sv.getHoten() + "</td>");
                                                out.print("<td>" + sv.getNgaysinh() + "</td>");
                                                out.print("<td>" + sv.getLop() + "</td>");
                                                out.print("<td></td>");
                                                out.print("</tr>");

                                                i++;
                                            }
                                            out.print("</tbody>");
                                        
      out.write("\n");
      out.write("\n");
      out.write("                                    </table>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- End of Page Wrapper -->\n");
      out.write("        ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <body>\n");
      out.write("        <!-- Scroll to Top Button-->\n");
      out.write("      <a class=\"scroll-to-top rounded\" href=\"#page-top\">\n");
      out.write("        <i class=\"fas fa-angle-up\"></i>\n");
      out.write("      </a>\n");
      out.write("\n");
      out.write("      <!-- Logout Modal-->\n");
      out.write("        <div class=\"modal fade\" id=\"logoutModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n");
      out.write("            <div class=\"modal-dialog\" role=\"document\">\n");
      out.write("                <div class=\"modal-content\">\n");
      out.write("                  <div class=\"modal-header\">\n");
      out.write("                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Ready to Leave?</h5>\n");
      out.write("                    <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">\n");
      out.write("                      <span aria-hidden=\"true\">×</span>\n");
      out.write("                    </button>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"modal-body\">Select \"Logout\" below if you are ready to end your current session.</div>\n");
      out.write("                  <div class=\"modal-footer\">\n");
      out.write("                    <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">Cancel</button>\n");
      out.write("                    <a class=\"btn btn-primary\" href=\"login.html\">Logout</a>\n");
      out.write("                  </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("      </div>\n");
      out.write("\n");
      out.write("      <!-- Bootstrap core JavaScript-->\n");
      out.write("      <script src=\"vendor/jquery/jquery.min.js\"></script>\n");
      out.write("      <script src=\"vendor/bootstrap/js/bootstrap.bundle.min.js\"></script>\n");
      out.write("\n");
      out.write("      <!-- Core plugin JavaScript-->\n");
      out.write("      <script src=\"vendor/jquery-easing/jquery.easing.min.js\"></script>\n");
      out.write("\n");
      out.write("      <!-- Custom scripts for all pages-->\n");
      out.write("      <script src=\"js/sb-admin-2.min.js\"></script>\n");
      out.write("\n");
      out.write("      <!-- Page level plugins -->\n");
      out.write("      <script src=\"vendor/chart.js/Chart.min.js\"></script>\n");
      out.write("\n");
      out.write("      <!-- Page level custom scripts -->\n");
      out.write("      <script src=\"js/demo/chart-area-demo.js\"></script>\n");
      out.write("      <script src=\"js/demo/chart-pie-demo.js\"></script>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
