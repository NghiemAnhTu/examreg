<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thêm sinh viên mới</font></font></h1>
                </div>
                <form action="themsinhvien" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <h6>Mã sinh viên:</h6>
                            <input type="number" class="form-control form-control-user" name="masv" placeholder="ID Student" required>
                        </div>

                        <div class="col-sm-6">
                            <h6>Lớp:</h6>
                            <input type="text" class="form-control form-control-user" name="lop" placeholder="Class of Student" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <h6>Họ tên sinh viên:</h6>
                        <input type="text" class="form-control form-control-user" name="hoten" placeholder="Fullname of Student" required>
                    </div>
                    <div class="form-group">
                        <h6>Địa chỉ sinh viên:</h6>
                        <input type="text" class="form-control form-control-user" name="diachi" placeholder="Address of Student" required>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <h6>Ngày sinh:</h6>
                            <input type="date" class="form-control form-control-user" name="ngaysinh" placeholder="Brithday of Student" required>
                        </div>
                        <div class="col-sm-6">
                            <h6>Khoa:</h6>
                            <input type="text" class="form-control form-control-user" name="khoa" placeholder="Factury Of Student" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block" name="btnthemsinhvien">Thêm</button>
                    
                </form>
                <hr>

            </div>

        
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
