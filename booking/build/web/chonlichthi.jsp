<%@page import="Model.LichThiModel"%>
<%@page import="Model.CaThiModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.CaThiDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header-user.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar-user.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5" style="height: 150px">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách lịch thi</font></font></h1>
                </div>
                <form action="DanhSachLichThi" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="smalt" placeholder="ID Search" value="<%=((String) request.getAttribute("IDLT") == null) ? "" :(String) request.getAttribute("IDLT")%>">
                        </div>

                        <div class="col-sm-6">
                            <button type="submit" name="chonlt" class="btn btn-primary btn-user btn-block" style="width: 60px;margin-left: 450px" >Tìm</button>
                        </div>
                    </div>
                </form>
            </div>
            
            <!-- Main Content -->
            <div id="content">
                
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    
                    
                    <form action='DangKiCaThi' method='post'>
                        <%
                            String alertRp = (String) session.getAttribute("alertDK");
                            String messageResp = (String) session.getAttribute("messageDK");
                        %>
                        <c:if test="${not empty messageResp}">
                            <div class="alert alert-<%=alertRp%>">
                                <%=messageResp%>
                            </div>
                        </c:if>
                        
                <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                    <thead>
                                        <tr>
                                            <!--<th>Mã ca thi</th>-->
                                            <th>Mã môn học</th>
                                            <th>Tên môn học</th>
                                            <th>Ngày thi</th>
                                            <th>Thời gian bắt đầu</th>
                                            <th>Thời gian kết thúc</th>
                                            <th>Phòng thi</th>
                                            <th>Địa chỉ</th>
                                            <th>Đã đăng kí</th>
                                            <th>Số máy</th>
                                            <th>Chọn</th>
                                        </tr>
                                    </thead>
                                    
                                    
                                    <%
                                        out.print("<tbody>");
                                        CaThiDAO ctd = new CaThiDAO();
                                        ArrayList<LichThiModel> list = new ArrayList<LichThiModel>();
                                        ArrayList<LichThiModel> list1 = (ArrayList<LichThiModel>) request.getAttribute("listLT");
                                        if(list1.size() != 0){
                                            list = list1;
                                        }else{
                                            list = ctd.getAllLichThi();
                                        }
                                        int i = 0;
                                        
                                        while(i < list.size()){
                                            LichThiModel ct = list.get(i);
                                            out.print("<tr>");
//                                            out.print("<form action='DangKiCaThi' method='post'>");
                                            
                                            
//                                            out.print("<td>" + ct.getMact()+ "</td>");
                                            out.print("<td>" + ct.getMamh()+ "</td>");
                                            out.print("<td>" + ct.getTenmh()+ "</td>");
                                            out.print("<td>" + ct.getNgaythi()+ "</td>");
                                            out.print("<td>" + ct.getBatdau()+ "</td>");
                                            out.print("<td>" + ct.getKetthuc()+ "</td>");
                                            out.print("<td>" + ct.getMapt()+ "</td>");
                                            out.print("<td>" + ct.getDiachi()+ "</td>");
                                            out.print("<td>" + ct.getDadangki()+ "</td>");
                                            out.print("<td>" + ct.getSomay()+ "</td>");
                                            out.print("<td><button type='submit' name='btnchon' value='" + ct.getMact() + "," + ct.getMamh() + "' class='btn btn-primary btn-user btn-block'>Chon</button></td>");
//                                            out.print("</form>");
                                            out.print("</tr>");
                                            
                                            i++;
                                        }
                                        out.print("</tbody>");
                                    %>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

                
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
