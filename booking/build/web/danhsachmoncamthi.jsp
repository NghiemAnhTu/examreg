<%@page import="DAO.Impl.SinhVienDAO"%>
<%@page import="Model.MonHocModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.MonHocDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header-user.jsp" %>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <%@include file="sidebar-user.jsp" %>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <div class="p-5" style="height: 150px">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách môn học bị cấm thi</font></font></h1>
                    </div>
                    
                </div>
                <!-- Main Content -->
                <div id="content">
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Mã môn học</th>
                                                <th>Tên môn học</th>
                                                <th>Số tín chỉ</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <%
                                                String smasv = (String) session.getAttribute("ID");
                                                int masv = Integer.parseInt(smasv);
                                                SinhVienDAO svd = new SinhVienDAO();
                                                ArrayList<MonHocModel> list = svd.getAllMonHocCamThi(masv);
//                                                out.print(masv + "|" + list.size());
                                                int i = 0;
                                                while(i < list.size()){
                                                    MonHocModel pt = list.get(i);
                                                    out.print("<tr>");
                                                    out.print("<td>" + pt.getMamh() + "</td>");
                                                    out.print("<td>" + pt.getTenmh() + "</td>");
                                                    out.print("<td>" + pt.getSotinchi() + "</td>");
                                                    out.print("</tr>");
                                                    
                                                    i++;
                                                }
                                            %>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>



        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- End of Page Wrapper -->
        <%@include file="footer.jsp" %>

    </body>

</html>
