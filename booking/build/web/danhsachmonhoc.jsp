<%@page import="Model.MonHocModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.MonHocDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <%@include file="sidebar.jsp" %>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <div class="p-5" style="height: 150px">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách môn học</font></font></h1>
                    </div>
                    <form action="TimKiemMaMh" method="post">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user" name="mamhs" value="<%=((String) request.getAttribute("Imamh") == null) ? "" :(String) request.getAttribute("Imamh")%>">
                            </div>

                            <div class="col-sm-6">
                                <button type="submit" id="timsinhvien" class="btn btn-primary btn-user btn-block" style="width: 60px;margin-left: 450px" >Tìm</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Main Content -->
                <div id="content">
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Mã môn học</th>
                                                <th>Tên môn học</th>
                                                <th>Số tín chỉ</th>
                                                <th>Chỉnh sửa</th>
                                                <th>Xóa</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <%
                                                MonHocDAO mhd = new MonHocDAO();
                                                String s = (String) request.getAttribute("Imamh");
                                                ArrayList<MonHocModel> list = new ArrayList<MonHocModel>();
                                                ArrayList<MonHocModel> list1 = (ArrayList<MonHocModel>) request.getAttribute("list");
                                                if(list1.size() != 0){
                                                    list = list1;
                                                }else{
                                                    list = mhd.getAllMonHoc();
                                                }
                                                
//                                                ArrayList<MonHocModel> list = mhd.getAllMonHoc();
                                                int i = 0;
                                                while (i < list.size()) {
                                                    MonHocModel pt = list.get(i);
                                                    out.print("<tr>");
                                                    out.print("<td>" + pt.getMamh() + "</td>");
                                                    out.print("<td>" + pt.getTenmh() + "</td>");
                                                    out.print("<td>" + pt.getSotinchi() + "</td>");
                                                    out.print("<td><a href='suamonhoc.jsp?suamamh=" + pt.getMamh() + "'>Sửa</a></td>");
                                                    out.print("<td><a href='Xoa?xoamamh=" + pt.getMamh() + "'>Xóa</a></div></td>");
                                                    //                                                out.print("<td><a class='del_monhoc' href='xoamonhoc.jsp?xoamamh=" + pt.getMamh() +"'>xóa</a</td>");
                                                    out.print("</tr>");
                                                    
                                                    i++;
                                                }
                                            %>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>



        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- End of Page Wrapper -->
        <%@include file="footer.jsp" %>

    </body>

</html>
