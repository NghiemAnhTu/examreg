<%@page import="Model.KiThiModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.KiThiDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5" style="height: 200px">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sach kì thi</font></font></h1>
                </div>
                <form action="DanhSachKiThi" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <h6>Nhập mã kì thi:</h6>
                            <input type="text" class="form-control form-control-user" name="makt" placeholder="ID Ki Thi" value="<%=((String) request.getAttribute("IDkt") == null) ? "" :(String) request.getAttribute("IDkt")%>">
                        </div>

                        <div class="col-sm-6">
                            <button type="submit" id="timkithi" class="btn btn-primary btn-user btn-block" style="width: 120px;margin-left: 400px" >Tìm</button>
                        </div>
                    </div>
                </form>
            </div>
            
            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">
                <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Mã kì thi</th>
                                            <th>Tên kì thi</th>
                                        </tr>
                                    </thead>
                                    <%
                                        out.print("<tbody>");
                                        KiThiDAO ktd = new KiThiDAO();
                                        ArrayList<KiThiModel> list = new ArrayList<KiThiModel>();
                                        ArrayList<KiThiModel> list1 = (ArrayList<KiThiModel>) request.getAttribute("listKT");
                                        if(list1.size() != 0){
                                            list = list1;
                                        }else{
                                            list = ktd.getAllKiThi();
                                        }
                                        int i = 0;
                                        while(i < list.size()){
                                            KiThiModel pt = list.get(i);
                                            out.print("<tr>");
                                            out.print("<td>" + pt.getMakt()+ "</td>");
                                            out.print("<td>" + pt.getTenkt()+ "</td>");
                                            out.print("</tr>");
                                            
                                            i++;
                                        }
                                        out.print("</tbody>");
                                    %>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
