<%@page import="Model.PhongThiModel"%>
<%@page import="DAO.Impl.PhongThiDAO"%>
<%@page import="Model.KiThiModel"%>
<%@page import="DAO.Impl.KiThiDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.MonHocModel"%>
<%@page import="DAO.Impl.MonHocDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thêm ca thi mới</font></font></h1>
            </div>
            <form action="themcathi" method="post">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Mã ca thi:</h6>
                        <input type="text" class="form-control form-control-user" name="mact" placeholder="ID Ca Thi" required>
                    </div>
                    
                    <div class="col-sm-6">
                        <h6>Mã kì thi:</h6>
                        <select class="form-control" name="makt" required>
                            <% 
                                KiThiDAO ktd = new KiThiDAO();
                                ArrayList<KiThiModel> listkt = ktd.getAllKiThi();
                                int i = 0;
                                out.print("<option>None</option>");
                                while(i < listkt.size()){
                                    out.print("<option>" + listkt.get(i).getMakt()+ "</option>");
                                    i++;
                                }
                            %>
                            
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Tên ca thi:</h6>
                        <input type="text" class="form-control form-control-user" name="tenct" placeholder="Name of Ca Thi" required>
                    </div>
                    <div class="col-sm-6">
                        <h6>Tên môn học:</h6>
                        <select class="form-control" name="mamh" required>
                            <% 
                                MonHocDAO mhd = new MonHocDAO();
                                ArrayList<MonHocModel> list = mhd.getAllMonHoc();
                                int j = 0;
                                out.print("<option>None</option>");
                                while(j < list.size()){
                                    out.print("<option>" + list.get(j).getMamh()+ "</option>");
                                    j++;
                                }
                            %>
                            
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Số lượng sinh viên đăng kí thi:</h6>
                        <input type="number" class="form-control form-control-user" name="soluongdk" placeholder="Nums" required>
                    </div>
                    <div class="col-sm-6">
                        <h6>Ngày thi:</h6>
                        <input type="date" class="form-control form-control-user" name="ngaythi" required>
                    </div>
                </div>
                      
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Thời gian bắt đầu:</h6>
                        <input type="time" class="form-control form-control-user" name="thoigianbatdau" required>
                    </div>
                    <div class="col-sm-6">
                        <h6>Thời gian kết thúc:</h6>
                        <input type="time" class="form-control form-control-user" name="thoigianketthuc" required>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Phòng thi:</h6>
                        <select class="form-control" name="mapt" required>
                            <% 
                                PhongThiDAO ptd = new PhongThiDAO();
                                ArrayList<PhongThiModel> listpt = ptd.getAllPhongThi();
                                int k = 0;
                                out.print("<option>None</option>");
                                while(k < list.size()){
                                    out.print("<option>" + listpt.get(k).getMapt()+  "</option>");
                                    k++;
                                }
                            %>
                            
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block" name="btnthemcathi">Thêm</button>
            </form>
            <hr>
            
        </div>
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
