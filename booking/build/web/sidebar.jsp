<%-- 
    Document   : sidebar
    Created on : Dec 3, 2019, 1:40:03 PM
    Author     : TuNghiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin-home.jsp">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">ExamReg Admin</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="admin-home.jsp">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            
            <!-- Quan ly mon thi -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <span>Quản lý tài khoản</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="login">Đăng xuất</a>
                    </div>
                </div>
            </li>
            
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Quan ly sinh vien -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                    <span>Quản lý sinh viên</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- Quan ly thông tin sinh viên -->
                        <h6 class="collapse-header">Thông tin sinh viên:</h6>
                        <a class="collapse-item" href="DanhSachSinhVien">Danh sách sinh viên</a>
                        <a class="collapse-item" href="themdssv">Thêm danh sach sinh viên</a>
                        <a class="collapse-item" href="themsinhvien.jsp">Thêm sinh viên</a>
                        <!-- Quan ly điều kiện thi -->
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Thông tin thi:</h6>
                        <a class="collapse-item" href="DanhSachThi">Danh sách dự thi</a>
                        <a class="collapse-item" href="DanhSachCamThi">Danh sách cấm thi</a>
                    </div>
                </div>
            </li>
            
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Quan ly công tác thi -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                    <span>Quản lý kì thi</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- Quan ly mon thi -->
                        <h6 class="collapse-header">Quản lý môn thi:</h6>
                        <a class="collapse-item" href="themmonhoc.jsp">Thêm môn thi</a>
                        <a class="collapse-item" href="TimKiemMaMh">Danh sách môn thi</a>
                        <!-- Quan ly ky thi -->
                        <h6 class="collapse-header">Quản lý kì thi:</h6>
                        <a class="collapse-item" href="taokithi">Tạo kì thi mới</a>
                        <a class="collapse-item" href="DanhSachKiThi">Danh sách các kì thi</a>
                        <!-- Quan ly ca thi -->
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Quản lý ca thi:</h6>
                        <a class="collapse-item" href="themcathi">Tạo ca thi mới</a>
                        <a class="collapse-item" href="DanhSachCaThi">Danh sách các ca thi</a>
                        <a class="collapse-item" href="DanhSachSVCT">Danh sách thi</a>
                        <!-- Quan ly phong thi -->
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Quản lý phòng thi:</h6>
                        <a class="collapse-item" href="TaoPhongThi">Tạo phòng thi mới</a>
                        <a class="collapse-item" href="DanhSachPhongThi">Danh sách các phòng thi</a>
                    </div>
                </div>
            </li>
            
            
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
    </body>
</html>
