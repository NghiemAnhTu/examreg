<%-- 
    Document   : admin-home
    Created on : Nov 25, 2019, 11:58:15 PM
    Author     : TuNghiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header-user.jsp" %>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <%@include file="sidebar-user.jsp" %>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <h1>Chào bạn đến với trang đăng kí lịch thi</h1>
                <% 
                    String masv = (String) session.getAttribute("ID");
                    out.print(masv);
                    
                %>
                
            </div>
            <!-- End of Content Wrapper -->

      </div>
      <!-- End of Page Wrapper -->
        <%@include file="footer.jsp" %>
    </body>

</html>
