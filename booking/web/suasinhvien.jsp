<%@page import="Model.SinhVienModel"%>
<%@page import="DAO.Impl.SinhVienDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <%
            int masv = Integer.parseInt(request.getParameter("emasv"));
            SinhVienDAO svd = new SinhVienDAO();
            SinhVienModel svm = svd.getSinhVien(masv);
        %>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sửa thông tin sinh viên</font></font></h1>
                </div>
                <form action="suasinhvien?emasv=<%=masv%>" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <h6>Mã sinh viên:</h6>
                            <input type="number" class="form-control form-control-user" name="masv" value="<%=svm.getMasv()%>" readonly>
                        </div>

                        <div class="col-sm-6">
                            <h6>Lớp:</h6>
                            <input type="text" class="form-control form-control-user" name="lop" value="<%=svm.getLop()%>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <h6>Họ tên sinh viên:</h6>
                        <input type="text" class="form-control form-control-user" name="hoten" value="<%=svm.getHoten()%>" required>
                    </div>
                    <div class="form-group">
                        <h6>Địa chỉ sinh viên:</h6>
                        <input type="text" class="form-control form-control-user" name="diachi" value="<%=svm.getDiachi()%>" required>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <h6>Ngày sinh:</h6>
                            <input type="date" class="form-control form-control-user" name="ngaysinh" value="<%=svm.getNgaysinh()%>" required>
                        </div>
                        <div class="col-sm-6">
                            <h6>Khoa:</h6>
                            <input type="text" class="form-control form-control-user" name="khoa" value="<%=svm.getKhoa()%>" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block" name="btnthemsinhvien">Sửa</button>
                </form>
                <hr>

            </div>
            
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
