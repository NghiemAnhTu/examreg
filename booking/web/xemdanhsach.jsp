<%@page import="DAO.Impl.CaThiDAO"%>
<%@page import="Model.SinhVienModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.SinhVienDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <%@include file="sidebar.jsp" %>
            <!-- End of Sidebar -->

            <%
                String mact = request.getParameter("vmact");
                CaThiDAO ctd = new CaThiDAO();
                ArrayList<SinhVienModel> list = ctd.getAllSinhVienInPhongThiOfCaThi(mact);
                
            %>
            <!-- Content Wrapper -->
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <div class="p-5" style="height: 200px">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách sinh viên</font></font></h1>
                    </div>
                    <form action="xemdanhsach.jsp?vmact=<%=mact%>" method="post">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <h6>Mã ca thi:</h6>
                                <input type="text" class="form-control form-control-user" name="smact" value="<%= mact%>" readonly>
                            </div>

                            <div class="col-sm-6">
                                <a href="#">In ra Word</a>
                                
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Main Content -->
                <div id="content">
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Mã sinh viên</th>
                                                <th>Họ tên</th>
                                                <th>Ngày sinh</th>
                                                <th>Lớp</th>
                                                <th>Ghi chú</th>
                                            </tr>
                                        </thead>
                                        <%
                                            out.print("<tbody>");
                                            int i = 0;
                                            while (i < list.size()) {
                                                SinhVienModel sv = list.get(i);
                                                out.print("<tr>");
                                                out.print("<td>" + sv.getMasv() + "</td>");
                                                out.print("<td>" + sv.getHoten() + "</td>");
                                                out.print("<td>" + sv.getNgaysinh() + "</td>");
                                                out.print("<td>" + sv.getLop() + "</td>");
                                                out.print("<td></td>");
                                                out.print("</tr>");

                                                i++;
                                            }
                                            out.print("</tbody>");
                                        %>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Page Wrapper -->
        <%@include file="footer.jsp" %>

    </body>

</html>
