<%-- 
    Document   : admin-home
    Created on : Nov 25, 2019, 11:58:15 PM
    Author     : TuNghiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <%@include file="sidebar.jsp" %>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <h1>Trang quản lí thi</h1>
            </div>
            <!-- End of Content Wrapper -->

      </div>
      <!-- End of Page Wrapper -->
        <%@include file="footer.jsp" %>
    </body>

</html>
