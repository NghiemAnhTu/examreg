<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thêm kì thi</font></font></h1>
            </div>
            <form action="taokithi" method="post">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Mã kì thi:</h6>
                        <input type="text" class="form-control form-control-user" name="makt" placeholder="ID Ki Thi" required>
                    </div>
                    <div class="col-sm-6">
                        <h6>Tên kì thi:</h6>
                        <input type="text" class="form-control form-control-user" name="tenkt" placeholder="Name of Ki Thi" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block" name="btnthemsinhvien">Thêm</button>
                
            </form>
            <hr>
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
