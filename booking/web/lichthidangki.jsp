<%@page import="Model.SinhVienThiModel"%>
<%@page import="Model.LichThiModel"%>
<%@page import="Model.CaThiModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.CaThiDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header-user.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar-user.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5" style="height: 150px">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách lịch thi đã đăng kí</font></font></h1>
                </div>
            </div>
            
            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">
                <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Mã sinh viên</th>
                                            <th>Họ và tên</th>
                                            <th>Lớp khóa học</th>
                                            <th>Mã môn học</th>
                                            <th>Tên môn học</th>
                                            <th>Ngày thi</th>
                                            <th>Thời gian bắt đầu</th>
                                            <th>Phòng thi</th>
                                            <th>In phiếu thi</th>
                                        </tr>
                                    </thead>
                                    
                                    <%
                                        out.print("<tbody>");
//                                        HttpSession session
                                        String masv = (String) session.getAttribute("ID");
                                        int msv = Integer.parseInt(masv);
                                        CaThiDAO ctd = new CaThiDAO();
                                        ArrayList<SinhVienThiModel> list = new ArrayList<SinhVienThiModel>();
                                        list = ctd.getLichSV1(msv);
                                        int i = 0;
                                        while(i < list.size()){
                                            SinhVienThiModel ct = list.get(i);
                                            out.print("<tr>");
                                            out.print("<td>" + ct.getMsv()+ "</td>");
                                            out.print("<td>" + ct.getHoten()+ "</td>");
                                            out.print("<td>" + ct.getLop()+ "</td>");
                                            out.print("<td>" + ct.getMamh()+ "</td>");
                                            out.print("<td>" + ct.getTenmh()+ "</td>");
                                            out.print("<td>" + ct.getNgaythi()+ "</td>");
                                            out.print("<td>" + ct.getBatdau()+ "</td>");
                                            out.print("<td>" + ct.getMapt()+ "</td>");
                                            out.print("<td><a href='#'>In</a</td>");
                                            out.print("</tr>");
                                            i++;
                                        }
                                        out.print("</tbody>");
                                    %>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
