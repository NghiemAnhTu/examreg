<%@page import="DAO.Impl.SinhVienDAO"%>
<%@page import="Model.SinhVienModel"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <%@include file="sidebar.jsp" %>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thêm danh sách sinh viên</font></font></h1>
                    </div>
                    <form action="themdssv" method="post">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="file"  name="filedanhsachsv" required>
                            </div>

                            <div class="col-sm-6">
                                <button type="submit" name="themfiledanhsachsv" class="btn btn-primary btn-user btn-block" style="width: 120px;margin-left: 400px" >Thêm</button>
                            </div>
                        </div>
                    </form>
                    <hr>

                </div>
                <!-- Main Content -->
                <div id="content">
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Mã sinh viên</th>
                                                <th>Họ tên</th>
                                                <th>Địa chỉ</th>
                                                <th>Ngày sinh</th>
                                                <th>Lớp</th>
                                                <th>Khoa</th>
                                                <th>Chỉnh sửa</th>
                                                <th>Xóa</th>
                                            </tr>
                                        </thead>
                                        <%
                                            out.print("<tbody>");
                                            String s = (String)request.getAttribute("file");
                                            SinhVienDAO svd = new SinhVienDAO();
                                            ArrayList<SinhVienModel> list = svd.getAllSinhVien();
                                            //ArrayList<SinhVienModel> list = (ArrayList<SinhVienModel>) request.getAttribute("DSSV");
//                                            int l = (int) request.getAttribute("sv");
                                            //out.print(s + "|" + list.size());
                                        int i = 0;
                                        while(i < list.size()){
                                            SinhVienModel sv = list.get(i);
                                            out.print("<tr>");
                                            out.print("<td>" + sv.getMasv() + "</td>");
                                            out.print("<td>" + sv.getHoten() + "</td>");
                                            out.print("<td>" + sv.getDiachi() + "</td>");
                                            out.print("<td>" + sv.getNgaysinh() + "</td>");
                                            out.print("<td>" + sv.getLop() + "</td>");
                                            out.print("<td>" + sv.getKhoa() + "</td>");
                                            out.print("<td><a href='suasinhvien.jsp?emasv=" + sv.getMasv() + "'>sửa</a></td>");
                                            out.print("<td><a href='XoaSV?dmasv=" + sv.getMasv() + "'>xóa</a></td>");
                                            out.print("</tr>");
                                            
                                            i++;
                                        }
                                        out.print("</tbody>");
                                        %>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->
        <%@include file="footer.jsp" %>

    </body>

</html>
