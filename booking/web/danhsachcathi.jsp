<%@page import="Model.CaThiModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.CaThiDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5" style="height: 150px">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách ca thi</font></font></h1>
                </div>
                <form action="DanhSachCaThi" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="smact" placeholder="ID Search" value="<%=((String) request.getAttribute("IDCT") == null) ? "" :(String) request.getAttribute("IDCT")%>">
                        </div>

                        <div class="col-sm-6">
                            <button type="submit" id="timct" class="btn btn-primary btn-user btn-block" style="width: 60px;margin-left: 450px" >Tìm</button>
                        </div>
                    </div>
                </form>
            </div>
            
            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">
                <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Mã ca thi</th>
                                            <th>Tên ca thi</th>
                                            <th>Mã kì thi</th>
                                            <th>Mã môn học</th>
                                            <th>Ngày thi</th>
                                            <th>Thời gian bắt đầu</th>
                                            <th>Thời gian kết thúc</th>
                                            <th>Phòng thi</th>
                                            <th>Số lượng đăng kí</th>
                                            <th>Chỉnh sửa</th>
                                            <th>Xóa</th>
                                            <th>Danh sách sinh viên</th>
                                        </tr>
                                    </thead>
                                    <%
                                        out.print("<tbody>");
                                        CaThiDAO ctd = new CaThiDAO();
                                        ArrayList<CaThiModel> list = new ArrayList<CaThiModel>();
                                        ArrayList<CaThiModel> list1 = (ArrayList<CaThiModel>) request.getAttribute("listCT");
                                        if(list1.size() != 0){
                                            list = list1;
                                        }else{
                                            list = ctd.getAllCaThi();
                                        }
                                        int i = 0;
                                        while(i < list.size()){
                                            CaThiModel ct = list.get(i);
                                            out.print("<tr>");
                                            out.print("<td>" + ct.getMact()+ "</td>");
                                            out.print("<td>" + ct.getTenct()+ "</td>");
                                            out.print("<td>" + ct.getMakt()+ "</td>");
                                            out.print("<td>" + ct.getMamh()+ "</td>");
                                            out.print("<td>" + ct.getNgaythi()+ "</td>");
                                            out.print("<td>" + ct.getBatdau()+ "</td>");
                                            out.print("<td>" + ct.getKetthuc()+ "</td>");
                                            out.print("<td>" + ct.getMapt()+ "</td>");
                                            out.print("<td>" + ct.getSoluongdk()+ "</td>");
                                            out.print("<td><a href='suacathi.jsp?emact=" + ct.getMact() + "'>sửa</a</td>");
                                            out.print("<td><a href='XoaCT?dmact=" + ct.getMact() + "'>xóa</a</td>");
                                            out.print("<td><a href='xemdanhsach.jsp?vmact=" + ct.getMact() + "'>xem</a</td>");
                                            out.print("</tr>");
                                            
                                            i++;
                                        }
                                        out.print("</tbody>");
                                    %>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
