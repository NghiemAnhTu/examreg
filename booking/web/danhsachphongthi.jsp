<%@page import="Model.PhongThiModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.PhongThiDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5" style="height: 150px">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách phòng thi</font></font></h1>
                </div>
                <form action="DanhSachPhongThi" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="mapt" value="<%=((String) request.getAttribute("IDPT") == null) ? "" :(String) request.getAttribute("IDPT")%>">
                        </div>

                        <div class="col-sm-6">
                            <button type="submit" id="timsinhvien" class="btn btn-primary btn-user btn-block" style="width: 60px;margin-left: 450px" >Tìm</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">
                <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Mã phòng thi</th>
                                            <th>Tên phòng thi</th>
                                            <th>Địa chỉ phòng thi</th>
                                            <th>Số máy</th>
                                            <th>Chỉnh sửa</th>
                                            <th>Xóa</th>
                                        </tr>
                                    </thead>
                                    <%
                                        out.print("<tbody>");
                                        PhongThiDAO ptd = new PhongThiDAO();
                                        ArrayList<PhongThiModel> list = new ArrayList<PhongThiModel>();
                                        ArrayList<PhongThiModel> list1 = (ArrayList<PhongThiModel>) request.getAttribute("listPT");
                                        if(list1.size() != 0){
                                            list = list1;
                                        }else{
                                            list = ptd.getAllPhongThi();
                                        }
                                        int i = 0;
                                        while(i < list.size()){
                                            PhongThiModel pt = list.get(i);
                                            out.print("<tr>");
                                            out.print("<td>" + pt.getMapt()+ "</td>");
                                            out.print("<td>" + pt.getTenpt()+ "</td>");
                                            out.print("<td>" + pt.getDiachi() + "</td>");
                                            out.print("<td>" + pt.getSomay()+ "</td>");
                                            out.print("<td><a href='suaphongthi.jsp?emapt=" + pt.getMapt()+ "'>sửa</a></td>");
                                            out.print("<td><a href='XoaPhongThi?dmapt=" + pt.getMapt()+ "'>xóa</a></td>");
                                            out.print("</tr>");
                                            
                                            i++;
                                        }
                                        out.print("</tbody>");
                                    %>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
