<%@page import="Model.PhongThiModel"%>
<%@page import="DAO.Impl.PhongThiDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
        <%
            String mapt = request.getParameter("dmapt");
            PhongThiDAO ptd = new PhongThiDAO();
            PhongThiModel ptm = ptd.getPhongThi(mapt);
        %>
        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Xóa phòng thi</font></font></h1>
            </div>
            <form action="XoaPhongThi?dmapt=<%=mapt%>" method="post">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Mã phòng thi:</h6>
                        <input type="text" class="form-control form-control-user" name="mapt" value="<%=ptm.getMapt() %>">
                    </div>
                    
                    <div class="col-sm-6">
                        <h6>Địa chỉ:</h6>
                        <input type="text" class="form-control form-control-user" name="diachi" value="<%=ptm.getDiachi() %>">
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Tên phòng thi:</h6>
                        <input type="text" class="form-control form-control-user" name="tenpt" value="<%=ptm.getTenpt() %>">
                    </div>
                    <div class="col-sm-6">
                        <h6>Số máy:</h6>
                        <input type="number" class="form-control form-control-user" name="somay" value="<%=ptm.getSomay()%>">
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary btn-user btn-block" name="btnxoaphongthi">Xóa</button>
            </form>
            <hr>
            
        </div>
            
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
