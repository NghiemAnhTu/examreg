<%@page import="Model.SinhVienModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.SinhVienDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div class="p-5" style="height: 150px">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách sinh viên</font></font></h1>
                </div>
                <form action="DanhSachSinhVien" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="masvs" value="<%=((String) request.getAttribute("SVids") == null) ? "" :(String) request.getAttribute("SVids")%>">
                        </div>

                        <div class="col-sm-6">
                            <button type="submit" id="timmonhoc" class="btn btn-primary btn-user btn-block" style="width: 60px;margin-left: 450px" >Tìm</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">
                <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Mã sinh viên</th>
                                            <th>Họ tên</th>
                                            <th>Địa chỉ</th>
                                            <th>Ngày sinh</th>
                                            <th>Lớp</th>
                                            <th>Khoa</th>
                                            <th>Chỉnh sửa</th>
                                            <th>Xóa</th>
                                        </tr>
                                    </thead>
                                    <%
                                        out.print("<tbody>");
                                        String id = (String) request.getAttribute("SVids");
                                        
                                        ArrayList<SinhVienModel> list = (ArrayList<SinhVienModel>)request.getAttribute("SVdetail");
                                        
                                        int i = 0;
                                        while(i < list.size()){
                                            SinhVienModel sv = list.get(i);
                                            out.print("<tr>");
                                            out.print("<td>" + sv.getMasv() + "</td>");
                                            out.print("<td>" + sv.getHoten() + "</td>");
                                            out.print("<td>" + sv.getDiachi() + "</td>");
                                            out.print("<td>" + sv.getNgaysinh() + "</td>");
                                            out.print("<td>" + sv.getLop() + "</td>");
                                            out.print("<td>" + sv.getKhoa() + "</td>");
                                            out.print("<td><a href='suasinhvien.jsp?emasv=" + sv.getMasv() + "'>sửa</a</td>");
                                            out.print("<td><a href='XoaSV?dmasv=" + sv.getMasv() + "'>xóa</a</td>");
                                            out.print("</tr>");
                                            
                                            i++;
                                        }
                                        out.print("</tbody>");
                                    %>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
