<%@page import="Model.SinhVienMonHocModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Impl.MonHocDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="p-5" style="height: 150px">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách sinh viên dự thi</font></font></h1>
                </div>
                <form action="DanhSachThi" method="post">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="file"  name="filethi" required>
                        </div>

                        <div class="col-sm-6">
                            <button type="submit" name="themfilethi" class="btn btn-primary btn-user btn-block" style="width: 120px;margin-left: 400px" >Thêm</button>
                        </div>
                    </div>
                </form>
            </div>
            
            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">
                <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Mã sinh viên</th>
                                            <th>Mã môn học</th>
                                            <th>Trạng thái thi</th>
                                        </tr>
                                    </thead>
                                    <%
                                        out.print("<tbody>");
                                        MonHocDAO mhd = new MonHocDAO();
                                        ArrayList<SinhVienMonHocModel> list = mhd.getDS(1);
                                        int i = 0;
                                        while(i < list.size()){
                                            SinhVienMonHocModel pt = list.get(i);
                                            out.print("<tr>");
                                            out.print("<td>" + pt.getMasv()+ "</td>");
                                            out.print("<td>" + pt.getMamh()+ "</td>");
                                            out.print("<td>Dự thi</td>");
                                            out.print("</tr>");
                                            
                                            i++;
                                        }
                                        out.print("</tbody>");
                                    %>
                                   
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
