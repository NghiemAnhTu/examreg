<%@page import="Model.MonHocModel"%>
<%@page import="DAO.Impl.MonHocDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <%@include file="header.jsp" %>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <%@include file="sidebar.jsp" %>
        <!-- End of Sidebar -->

        <!--- lay mamh -->
        <%
            String mamh = request.getParameter("xoamamh");
            MonHocDAO mhd = new MonHocDAO();
            MonHocModel mhm = mhd.getDetailMonHoc(mamh);
        %>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Xóa môn thi</font></font></h1>
            </div>
                <form action="Xoa?xoamamh=<%=mamh%>" method="post">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <h6>Mã môn học:</h6>
                        <input type="text" class="form-control form-control-user" id="mamh" name="inpmamh" placeholder="Nhập mã môn học" value="<%= mhm.getMamh()%>" readonly>
                    </div>
                    
                    <div class="col-sm-6">
                        <h6>Số tín chỉ:</h6>
                        <input type="number" class="form-control form-control-user" name="inpsotinchi" placeholder="Nhập số tín chỉ" value="<%=mhm.getSotinchi() %>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <h6>Tên môn học:</h6>
                    <input type="text" class="form-control form-control-user" name="inptenmh" placeholder="Nhập tên môn học" value="<%= mhm.getTenmh() %>" readonly>
                </div>
                
                <button type="submit" class="btn btn-primary btn-user btn-block" name="btnxoamonhoc" style="width: 200px" >Xóa</button>
                
            </form>
            <hr>
            
        </div>
            
        <!-- End of Footer -->

        </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <%@include file="footer.jsp" %>
    
    </body>

</html>
