/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.ICaThiDAO;
import Model.CaThiModel;
import Model.CaThiPhongThiModel;
import Model.LichThiModel;
import Model.MonHocModel;
import Model.SinhVienModel;
import Model.SinhVienThiModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class CaThiDAO implements ICaThiDAO{
    
    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    }

    @Override
    public ArrayList<CaThiModel> getAllCaThi() {
        ArrayList<CaThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT cathi.*, cathi_phongthi.mapt FROM cathi INNER JOIN cathi_phongthi ON cathi.mact = cathi_phongthi.mact;";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                CaThiModel ct = new CaThiModel();
                ct.setMact(rs.getString("mact"));
                ct.setTenct(rs.getString("tenct"));
                ct.setMakt(rs.getString("makt"));
                ct.setMamh(rs.getString("mamh"));
                ct.setNgaythi(rs.getString("ngaythi"));
                ct.setBatdau(rs.getString("batdau"));
                ct.setKetthuc(rs.getString("ketthuc"));
                ct.setSoluongdk(rs.getInt("soluongdk"));
                ct.setMapt(rs.getString(9));
                list.add(ct);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public CaThiModel getCaThi(String id) {
        CaThiModel ct = new CaThiModel();
        try {
            String sql = "SELECT * FROM cathi WHERE mact = '" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                ct.setMact(rs.getString("mact"));
                ct.setTenct(rs.getString("tenct"));
                ct.setMakt(rs.getString("makt"));
                ct.setMamh(rs.getString("mamh"));
                ct.setNgaythi(rs.getString("ngaythi"));
                ct.setBatdau(rs.getString("batdau"));
                ct.setKetthuc(rs.getString("ketthuc"));
                ct.setSoluongdk(rs.getInt("soluongdk"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ct;
    }

    @Override
    public boolean addCaThi(CaThiModel ct) {
        int count = 0;
        try {
            String sql = "INSERT INTO cathi(mact, tenct, makt, mamh, ngaythi, batdau, ketthuc, soluongdk) VALUES('" + ct.getMact() + "','"
                    + ct.getTenct() + "','" + ct.getMakt() + "','" + ct.getMamh() + "','" + ct.getNgaythi() + "','" + ct.getBatdau() + "','"
                    + ct.getKetthuc() + "'," + ct.getSoluongdk() + ");";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            System.out.println(sql);
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean editCaThi(String id, CaThiModel ct) {
        int count = 0;
        try {
            String sql = "UPDATE cathi SET mact ='" + ct.getMact() + "', tenct ='" + ct.getTenct() + "', makt='" + ct.getMakt()
                    + "', mamh ='" + ct.getMamh() + "', ngaythi ='" + ct.getNgaythi() + "', batdau ='" + ct.getBatdau() + "', ketthuc ='"
                    + ct.getKetthuc() + "', soluongdk =" + ct.getSoluongdk() + " WHERE mact ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean deleteCaThi(String id) {
        int count = 0;
        try {
            String sql = "DELETE FROM cathi WHERE mact ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public ArrayList<SinhVienModel> getAllSinhVienInPhongThiOfCaThi(String idct) {
        ArrayList<SinhVienModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT sinhvien.* FROM sinhvien INNER JOIN sinhvien_cathi ON sinhvien_cathi.masv = sinhvien.masv "
                    + "WHERE sinhvien_cathi.mact ='" + idct +"'";
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                SinhVienModel sv = new SinhVienModel();
                sv.setMasv(rs.getInt("masv"));
                sv.setHoten(rs.getString("hoten"));
                sv.setDiachi(rs.getString("diachi"));
                sv.setNgaysinh(rs.getString("ngaysinh").toString());
                sv.setLop(rs.getString("lop"));
                sv.setKhoa(rs.getString("khoa"));
                list.add(sv);
                
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<MonHocModel> getAllMonHocOfNgayThi(String idnt) {
        ArrayList<MonHocModel> list = new ArrayList<>();
        try {
            String sql = "SELECT monhoc.* FROM monhoc INNER JOIN cathi ON cathi.mamh = monhoc.mamh"
                    + " INNER JOIN lichthi ON cathi.malt = lichthi.malt"
                    + " WHERE lichthi.mant ='" + java.sql.Date.valueOf(idnt) + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                MonHocModel mh = new MonHocModel();
                mh.setMamh(rs.getString("mamh"));
                mh.setTenmh(rs.getString("tenmh"));
                mh.setSotinchi(rs.getInt("sotinchi"));
                list.add(mh);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public ArrayList<CaThiModel> getDetailsAllCaThi(String id) {
        ArrayList<CaThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT cathi.*, cathi_phongthi.mapt FROM cathi INNER JOIN cathi_phongthi ON cathi.mact = cathi_phongthi.mact WHERE cathi.mamh like '%" + id + "%';" ;
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                CaThiModel ct = new CaThiModel();
                ct.setMact(rs.getString("mact"));
                ct.setTenct(rs.getString("tenct"));
                ct.setMakt(rs.getString("makt"));
                ct.setMamh(rs.getString("mamh"));
                ct.setNgaythi(rs.getString("ngaythi"));
                ct.setBatdau(rs.getString("batdau"));
                ct.setKetthuc(rs.getString("ketthuc"));
                ct.setSoluongdk(rs.getInt("soluongdk"));
                ct.setMapt(rs.getString(9));
                list.add(ct);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public ArrayList<LichThiModel> getDetailsCaThiPT(String mamh) {
        ArrayList<LichThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT cathi.mamh, monhoc.tenmh, cathi.ngaythi, cathi.batdau, cathi.ketthuc, phongthi.tenpt, phongthi.diachi, cathi.mact, phongthi.mapt, cathi.soluongdk, phongthi.somay FROM cathi INNER JOIN cathi_phongthi ON cathi_phongthi.mact = cathi.mact"
                    + " INNER JOIN phongthi ON phongthi.mapt = cathi_phongthi.mapt"
                    + " INNER JOIN monhoc ON monhoc.mamh = cathi.mamh"
                    + " WHERE cathi.mamh like '%" + mamh +"%';";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                LichThiModel lt = new LichThiModel();
                lt.setMamh(rs.getString(1));
                lt.setTenmh(rs.getString(2));
                lt.setNgaythi(rs.getString(3));
                lt.setBatdau(rs.getString(4));
                lt.setKetthuc(rs.getString(5));
                lt.setTenpt(rs.getString(6));
                lt.setDiachi(rs.getString(7));
                lt.setMact(rs.getString(8));
                lt.setMapt(rs.getString(9));
                lt.setDadangki(rs.getInt(10));
                lt.setSomay(rs.getInt(11));
                list.add(lt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    // danh sach lich thi
    public ArrayList<LichThiModel> getAllLichThi() {
        ArrayList<LichThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT cathi.mamh, monhoc.tenmh, cathi.ngaythi, cathi.batdau, cathi.ketthuc, phongthi.tenpt, phongthi.diachi, cathi.mact, phongthi.mapt, cathi.soluongdk, phongthi.somay  FROM cathi INNER JOIN cathi_phongthi ON cathi_phongthi.mact = cathi.mact"
                    + " INNER JOIN phongthi ON phongthi.mapt = cathi_phongthi.mapt"
                    + " INNER JOIN monhoc ON monhoc.mamh = cathi.mamh;";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                LichThiModel lt = new LichThiModel();
                lt.setMamh(rs.getString(1));
                lt.setTenmh(rs.getString(2));
                lt.setNgaythi(rs.getString(3));
                lt.setBatdau(rs.getString(4));
                lt.setKetthuc(rs.getString(5));
                lt.setTenpt(rs.getString(6));
                lt.setDiachi(rs.getString(7));
                lt.setMact(rs.getString(8));
                lt.setMapt(rs.getString(9));
                lt.setDadangki(rs.getInt(10));
                lt.setSomay(rs.getInt(11));
                list.add(lt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    // danh sach lich thi sinh vien da dang ki
    public ArrayList<LichThiModel> getLichThiSV(int msv) {
        ArrayList<LichThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT cathi.mamh, monhoc.tenmh, cathi.ngaythi, cathi.batdau, cathi.ketthuc, phongthi.tenpt, phongthi.diachi, cathi.mact, phongthi.mapt FROM cathi INNER JOIN cathi_phongthi ON cathi_phongthi.mact = cathi.mact"
                    + " INNER JOIN phongthi ON phongthi.mapt = cathi_phongthi.mapt"
                    + " INNER JOIN monhoc ON monhoc.mamh = cathi.mamh"
                    + " INNER JOIN sinhvien_cathi ON sinhvien_cathi.mact = cathi.mact WHERE sinhvien_cathi.masv =" + msv + ";";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                LichThiModel lt = new LichThiModel();
                lt.setMamh(rs.getString(1));
                lt.setTenmh(rs.getString(2));
                lt.setNgaythi(rs.getString(3));
                lt.setBatdau(rs.getString(4));
                lt.setKetthuc(rs.getString(5));
                lt.setTenpt(rs.getString(6));
                lt.setDiachi(rs.getString(7));
                lt.setMact(rs.getString(8));
                lt.setMapt(rs.getString(9));
                
                list.add(lt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public boolean addCaThiPhongThi(CaThiPhongThiModel ct) {
        int count = 0;
        try {
            String sql = "INSERT INTO cathi_phongthi(mact, mapt) VALUES('" + ct.getMact() + "','"
                    + ct.getMapt() + "');";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            System.out.println(sql);
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    public LichThiModel getLichThi(String id) {
        LichThiModel lt = new LichThiModel();
        try {
            String sql = "SELECT cathi.mamh, monhoc.tenmh, cathi.ngaythi, cathi.batdau, cathi.ketthuc, phongthi.tenpt, phongthi.diachi, cathi.mact, phongthi.mapt, cathi.soluongdk, phongthi.somay  FROM cathi INNER JOIN cathi_phongthi ON cathi_phongthi.mact = cathi.mact"
                    + " INNER JOIN phongthi ON phongthi.mapt = cathi_phongthi.mapt"
                    + " INNER JOIN monhoc ON monhoc.mamh = cathi.mamh"
                    + " INNER JOIN sinhvien_cathi ON sinhvien_cathi.mact = cathi.mact WHERE cathi.mact ='" + id + "';";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                lt.setMamh(rs.getString(1));
                lt.setTenmh(rs.getString(2));
                lt.setNgaythi(rs.getString(3));
                lt.setBatdau(rs.getString(4));
                lt.setKetthuc(rs.getString(5));
                lt.setTenpt(rs.getString(6));
                lt.setDiachi(rs.getString(7));
                lt.setMact(rs.getString(8));
                lt.setMapt(rs.getString(9));
                lt.setDadangki(rs.getInt(10));
                lt.setSomay(rs.getInt(11));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lt;
    }
    
    public ArrayList<SinhVienThiModel> getLichSV1(int msv) {
        ArrayList<SinhVienThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT sinhvien.masv, sinhvien.hoten, sinhvien.lop, monhoc.mamh, monhoc.tenmh, cathi.ngaythi, cathi.batdau, cathi_phongthi.mapt FROM sinhvien INNER JOIN sinhvien_cathi ON sinhvien.masv = sinhvien_cathi.masv"
                    + " INNER JOIN cathi ON cathi.mact = sinhvien_cathi.mact"
                    + " INNER JOIN monhoc ON monhoc.mamh = cathi.mamh"
                    + " INNER JOIN cathi_phongthi ON cathi.mact = cathi_phongthi.mact WHERE sinhvien.masv =" + msv + ";";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                SinhVienThiModel lt = new SinhVienThiModel();
                lt.setMsv(rs.getInt(1));
                lt.setHoten(rs.getString(2));
                lt.setLop(rs.getString(3));
                lt.setMamh(rs.getString(4));
                lt.setTenmh(rs.getString(5));
                lt.setNgaythi(rs.getString(6));
                lt.setBatdau(rs.getString(7));
                lt.setMapt(rs.getString(8));
                
                list.add(lt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
