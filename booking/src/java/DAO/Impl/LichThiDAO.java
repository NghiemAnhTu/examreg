/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.ILichThiDAO;
import Model.LichThiModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class LichThiDAO implements ILichThiDAO{
    
    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    }

    @Override
    public ArrayList<LichThiModel> getAllLichThi() {
        ArrayList<LichThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM lichthi";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                LichThiModel lt = new LichThiModel();
                lt.setMalt(rs.getString("malt"));
                lt.setMant(rs.getDate("mant").toString());
                lt.setBatdau(rs.getTime("batdau").toString());
                lt.setKetthuc(rs.getTime("ketthuc").toString());
                list.add(lt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public LichThiModel getLichThi(String id) {
        LichThiModel lt = new LichThiModel();
        try {
            String sql = "SELECT * FROM lichthi WHERE malt = " + id;
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                lt.setMalt(rs.getString("malt"));
                lt.setMant(rs.getDate("mant").toString());
                lt.setBatdau(rs.getTime("batdau").toString());
                lt.setKetthuc(rs.getTime("ketthuc").toString());
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lt;
    }

    @Override
    public boolean addLichThi(LichThiModel lt) {
        int count = 0;
        try {
            String sql = "INSERT INTO lichthi(malt, mant, batdau, ketthuc) VALUES(" + lt.getMalt() + "," + java.sql.Date.valueOf(lt.getMant())
                    + "," + java.sql.Time.valueOf(lt.getBatdau()) + "," + java.sql.Time.valueOf(lt.getKetthuc());
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(LichThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean editLichThi(String id, LichThiModel lt) {
        int count = 0;
        try {
            String sql = "UPDATE lichthi SET malt =" + lt.getMalt() + ", mant =" + java.sql.Date.valueOf(lt.getMant())
                    + ", batdau =" + java.sql.Time.valueOf(lt.getBatdau()) + ", ketthuc =" 
                    + java.sql.Time.valueOf(lt.getKetthuc()) + " WHERE malt =" + id;
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(LichThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean deleteLichThi(String id) {
        int count = 0;
        try {
            String sql = "DELETE FROM lichthi WHERE malt =" + id;
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(LichThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);}
    
}
