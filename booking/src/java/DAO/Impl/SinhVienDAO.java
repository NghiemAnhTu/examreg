/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.ISinhVienDAO;
import Model.CaThiModel;
import Model.MonHocModel;
import Model.SinhVienModel;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class SinhVienDAO implements ISinhVienDAO{
    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    }

    @Override
    public ArrayList<SinhVienModel> getAllSinhVien() {
        ArrayList<SinhVienModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM sinhvien";
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                SinhVienModel sv = new SinhVienModel();
                sv.setMasv(rs.getInt("masv"));
                sv.setHoten(rs.getString("hoten"));
                sv.setDiachi(rs.getString("diachi"));
                sv.setNgaysinh(rs.getString("ngaysinh"));
                sv.setLop(rs.getString("lop"));
                sv.setKhoa(rs.getString("khoa"));
                list.add(sv);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public SinhVienModel getSinhVien(int id) {
        SinhVienModel sv = new SinhVienModel();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM sinhvien WHERE masv=" + id;
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                sv.setMasv(rs.getInt("masv"));
                sv.setHoten(rs.getString("hoten"));
                sv.setDiachi(rs.getString("diachi"));
                sv.setNgaysinh(rs.getString("ngaysinh"));
                sv.setLop(rs.getString("lop"));
                sv.setKhoa(rs.getString("khoa"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sv;
    }
    
    public ArrayList<SinhVienModel> getListDetailSinhVien(int id) {
        ArrayList<SinhVienModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM sinhvien WHERE masv like " + id +"%";
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                SinhVienModel sv = new SinhVienModel();
                sv.setMasv(rs.getInt("masv"));
                sv.setHoten(rs.getString("hoten"));
                sv.setDiachi(rs.getString("diachi"));
                sv.setNgaysinh(rs.getString("ngaysinh"));
                sv.setLop(rs.getString("lop"));
                sv.setKhoa(rs.getString("khoa"));
                list.add(sv);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public boolean editSinhVien(int id, SinhVienModel sv) {
        int count = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            
            String sql = "UPDATE sinhvien SET masv =" + sv.getMasv() + ", hoten ='" + sv.getHoten()
                    + "', diachi ='" + sv.getDiachi() + "', ngaysinh ='" + sv.getNgaysinh()
                    + "', lop ='" + sv.getLop() + "', khoa ='" + sv.getKhoa() + "' WHERE masv =" + id;
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count ==0) ? false : true;
    }

    @Override
    public boolean addSinhVien(SinhVienModel sv) {
        int count = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "INSERT into sinhvien(masv,hoten,diachi, ngaysinh, lop, khoa) values(" + sv.getMasv() + ", '" + sv.getHoten()
                    + "', '" + sv.getDiachi() + "','" + sv.getNgaysinh()
                    + "', '" + sv.getLop() + "', '" + sv.getKhoa() + "');";
            System.out.println(sql);
            
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false : true;
    }

    @Override
    public boolean deleteSinhVien(int id) {
        int count = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            
            String sql = "DELETE FROM sinhvien WHERE masv =" + id;
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count ==0) ? false : true;
    }

    @Override
    public ArrayList<MonHocModel> getAllMonHocThi(int id) {
        ArrayList<MonHocModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT monhoc.* FROM monhoc INNER JOIN sinhvien_monhoc ON monhoc.mamh = sinhvien_monhoc.mamh "
                    + "WHERE sinhvien_monhoc.trangthai = 1 AND sinhvien_monhoc.masv =" + id;
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                MonHocModel mh = new MonHocModel();
                mh.setMamh(rs.getString("mamh"));
                mh.setTenmh(rs.getString("tenmh"));
                mh.setSotinchi(rs.getInt("sotinchi"));
                list.add(mh);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    @Override
    public ArrayList<MonHocModel> getAllMonHocCamThi(int id) {
        ArrayList<MonHocModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT monhoc.* FROM monhoc INNER JOIN sinhvien_monhoc ON monhoc.mamh = sinhvien_monhoc.mamh "
                    + "WHERE sinhvien_monhoc.trangthai = 0 AND sinhvien_monhoc.masv =" + id;
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                MonHocModel mh = new MonHocModel();
                mh.setMamh(rs.getString("mamh"));
                mh.setTenmh(rs.getString("tenmh"));
                mh.setSotinchi(rs.getInt("sotinchi"));
                list.add(mh);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<CaThiModel> getAllCaThi(int id) {
        ArrayList<CaThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT cathi.* FROM cathi INNER JOIN sinhvien_cathi ON cathi.mact = sinhvien_cathi.mact"
                    + " WHERE sinhvien_cathi.masv =" + id;
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                CaThiModel ct = new CaThiModel();
                ct.setMact(rs.getString("mact"));
                ct.setTenct(rs.getString("tenct"));
                ct.setMakt(rs.getString("makt"));
                ct.setMamh(rs.getString("mamh"));
                ct.setNgaythi(rs.getString("ngaythi"));
                ct.setBatdau(rs.getString("batdau"));
                ct.setKetthuc(rs.getString("ketthuc"));
                ct.setSoluongdk(rs.getInt("soluongdk"));
                list.add(ct);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
 
    
    public boolean addDSSinhVien(String sql) {
        int count = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            
            count = st.executeUpdate(sql);
            System.out.println("1111111111");
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false : true;
    }
}
