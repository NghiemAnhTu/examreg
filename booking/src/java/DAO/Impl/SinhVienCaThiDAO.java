/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.ISinhVienCaThiDAO;
import Model.CaThiModel;
import Model.LichThiModel;
import Model.SinhVienCaThiModel;
import Model.SinhVienModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class SinhVienCaThiDAO implements ISinhVienCaThiDAO {

    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    }
    
    @Override
    public ArrayList<CaThiModel> getAllCaThi(int id) {
        ArrayList<CaThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT cathi.* FROM cathi INNER JOIN sinhvien_cathi ON sinhvien_cathi.mact = cathi.mact"
                   + " WHERE sinhvien_cathi.masv =" + id;
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                CaThiModel ct = new CaThiModel();
                ct.setMact(rs.getString("mact"));
                ct.setTenct(rs.getString("tenct"));
                ct.setMakt(rs.getString("makt"));
                ct.setMamh(rs.getString("mamh"));
                ct.setNgaythi(rs.getString("ngaythi"));
                ct.setBatdau(rs.getString("batdau"));
                ct.setKetthuc(rs.getString("ketthuc"));
                ct.setSoluongdk(rs.getInt("soluongdk"));
                list.add(ct);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public boolean addSinhVienCaThi(SinhVienCaThiModel sv) {
        int masv = sv.getMasv();
        String mact = sv.getMact();
        CaThiDAO ctd = new CaThiDAO();
        CaThiModel ctm = ctd.getCaThi(mact);
        int soluongdk = ctm.getSoluongdk();
        int count = 0, count1 = 0;
        try {
            String sqlInsert = "INSERT INTO sinhvien_cathi(masv, mact) VALUES(" + masv + ",'" + mact + "');";
            String sqlUpdate = "UPDATE cathi SET soluongdk =" + (soluongdk + 1)
                    + " WHERE mact = '" + mact + "';";
            System.out.println(sqlInsert + "|" + sqlUpdate);
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sqlInsert);
            if(count != 0){
                count1 = st.executeUpdate(sqlUpdate);
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienCaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean editSinhVienCaThi(SinhVienCaThiModel old, SinhVienCaThiModel newO) {
        CaThiDAO ctd = new CaThiDAO();
        CaThiModel cto = ctd.getCaThi(old.getMact());
        int soluongdko = cto.getSoluongdk();
        CaThiModel ctm = ctd.getCaThi(newO.getMact());
        int soluongn = ctm.getSoluongdk();
        int count = 0;
        try {
            String sql = "";
            if(old.getMact().equals(newO.getMact())){
                sql = "UPDATE sinhvien_cathi SET masv =" + newO.getMasv() + " WHERE masv =" + old.getMasv();
            }else{
                sql = "UPDATE sinhvien_cathi SET masv =" + newO.getMasv() + ", mact =" + newO.getMact() 
                        + " WHERE masv =" + old.getMasv() + " AND mact =" + old.getMact() + ";"
                        + "UPDATE cathi SET soluongdk =" + (soluongn + 1) + " WHERE mact = " + newO.getMact() + ";"
                        + "UPDATE cathi SET soluongdk =" + (soluongdko - 1) + " WHERE mact = " + old.getMact() + ";";
                        
            }
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienCaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public ArrayList<SinhVienModel> getAllSinhVienThi(String mact) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteSinhVienCaThi(SinhVienCaThiModel sv) {
        CaThiDAO ctd = new CaThiDAO();
        CaThiModel ctm = ctd.getCaThi(sv.getMact());
        int soluongdk = ctm.getSoluongdk();
        int count = 0;
        try {
            String sql = "DELETE FROM sinhvien_cathi WHERE masv =" + sv.getMasv() + " AND mact =" + sv.getMact() + ";"
                    + "UPDATE cathi SET soluongdk =" + (soluongdk - 1)
                    + " WHERE mact = " + sv.getMact() + ";";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienCaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }
    
    public boolean KiemTraDangKi(int msv, String mamh){
        String ma = "";
        try {
            String sql = "SELECT * FROM sinhvien_cathi WHERE masv =" + msv + " AND mact like '%" + mamh + "%';";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                ma = rs.getString(2); 
                System.out.println("AAAAAAAA" + ma);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienCaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (ma.length() == 0 ) ? true : false;
    } 
    
    public boolean KiemTraSinhVienHoc(int msv, String mamh){
        String ma = "";
        try {
            String sql = "SELECT * FROM sinhvien_monhoc WHERE trangthai = 1 AND masv =" + msv + " AND mamh ='" + mamh + "';";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                ma = rs.getString(1); 
            }
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienCaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (ma.length() != 0 ) ? true : false;
    }
    
    // kiem tra so luong cho trong
    public boolean KiemTraSoLuongCho(String mact){
        int dk = 0;
        int somay = 0;
        try {
            String sql = "SELECT cathi.soluongdk, phongthi.somay FROM cathi INNER JOIN cathi_phongthi ON cathi.mact = cathi_phongthi.mact"
                    + " INNER JOIN phongthi ON phongthi.mapt = cathi_phongthi.mapt"
                    + " WHERE cathi.mact = '" + mact + "';";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                dk = rs.getInt(1);
                somay = rs.getInt(2);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienCaThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(somay > dk){
            return true;
        } 
        return false;
    }
}
