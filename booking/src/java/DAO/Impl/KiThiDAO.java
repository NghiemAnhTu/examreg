/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.IKiThiDAO;
import Model.KiThiModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class KiThiDAO implements IKiThiDAO{
    
    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    }

    @Override
    public ArrayList<KiThiModel> getAllKiThi() {
        ArrayList<KiThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM kithi";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                KiThiModel kt = new KiThiModel();
                kt.setMakt(rs.getString("makt"));
                kt.setTenkt(rs.getString("tenkt"));
                list.add(kt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public KiThiModel getKiThi(String id) {
        KiThiModel kt = new KiThiModel();
        try {
            String sql = "SELECT * FROM kithi WHERE makt ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                kt.setMakt(rs.getString("makt"));
                kt.setTenkt(rs.getString("tenkt"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kt;
    }

    @Override
    public boolean editKiThi(String id, KiThiModel kt) {
        int count = 0;
        try {
            String sql = "UPDATE kithi SET makt ='" + kt.getMakt() + "', tenkt ='" + kt.getTenkt()
                    + "' WHERE makt ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(KiThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean addKiThi(KiThiModel kt) {
        int count = 0;
        try {
            String sql = "INSERT INTO kithi(makt, tenkt) VALUES('" + kt.getMakt() + "','" + kt.getTenkt() + "');";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(KiThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean deleteKiThi(String id) {
        int count = 0;
        try {
            String sql = "DELETE FROM kithi WHERE makt ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(KiThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }
    
    // search
    public ArrayList<KiThiModel> getDeltailsAllKiThi(String id) {
        ArrayList<KiThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM kithi WHERE makt like '" + id + "%'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                KiThiModel kt = new KiThiModel();
                kt.setMakt(rs.getString("makt"));
                kt.setTenkt(rs.getString("tenkt"));
                list.add(kt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
}
