/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.IPhongThiDAO;
import Model.PhongThiModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class PhongThiDAO implements IPhongThiDAO{

    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    }
                 
    @Override
    public ArrayList<PhongThiModel> getAllPhongThi() {
        ArrayList<PhongThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM phongthi";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                PhongThiModel pt = new PhongThiModel();
                pt.setMapt(rs.getString("mapt"));
                pt.setTenpt(rs.getString("tenpt"));
                pt.setDiachi(rs.getString("diachi"));
                pt.setSomay(rs.getInt("somay"));
                list.add(pt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PhongThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public PhongThiModel getPhongThi(String id) {
        PhongThiModel pt = new PhongThiModel();
        try {
            String sql = "SELECT * FROM phongthi WHERE mapt = '" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                pt.setMapt(rs.getString("mapt"));
                pt.setTenpt(rs.getString("tenpt"));
                pt.setDiachi(rs.getString("diachi"));
                pt.setSomay(rs.getInt("somay"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PhongThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pt;
    }

    @Override
    public boolean addPhongThi(PhongThiModel ptm) {
        int count = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "INSERT into phongthi(mapt, tenpt, diachi, somay) values('" + ptm.getMapt() + "', '" + ptm.getTenpt()
                    + "', '" + ptm.getDiachi() + "', " + ptm.getSomay() +  ")";
            
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PhongThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false : true;
    }

    @Override
    public boolean deletePhongThi(String id) {
        int count = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "DELETE FROM phongthi WHERE mapt ='" + id + "'";
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PhongThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false : true;
    }

    @Override
    public boolean editPhongThi(String id, PhongThiModel ptm) {
        int count = 0;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "UPDATE phongthi SET mapt ='" + ptm.getMapt() + "', tenpt ='" + ptm.getTenpt()
                    + "', diachi ='" + ptm.getDiachi() + "', somay =" + ptm.getSomay() + " WHERE mapt ='" + id + "'";
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PhongThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false : true;
    }
    
    public ArrayList<PhongThiModel> getDetailPhongThi(String id) {
        ArrayList<PhongThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM phongthi WHERE mapt like '" + id + "%'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                PhongThiModel pt = new PhongThiModel();
                pt.setMapt(rs.getString("mapt"));
                pt.setTenpt(rs.getString("tenpt"));
                pt.setDiachi(rs.getString("diachi"));
                pt.setSomay(rs.getInt("somay"));
                list.add(pt);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PhongThiDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    
}
