/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.IMonHocDAO;
import Model.CaThiModel;
import Model.MonHocModel;
import Model.SinhVienModel;
import Model.SinhVienMonHocModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class MonHocDAO implements IMonHocDAO{
    
    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    } 

    @Override
    public ArrayList<MonHocModel> getAllMonHoc() {
        ArrayList<MonHocModel> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM monhoc;";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                MonHocModel mh = new MonHocModel();
                mh.setMamh(rs.getString("mamh"));
                mh.setTenmh(rs.getString("tenmh"));
                mh.setSotinchi(rs.getInt("sotinchi"));
                list.add(mh);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public MonHocModel getDetailMonHoc(String id) {
        MonHocModel mh = new MonHocModel();
        try {
            String sql = "SELECT * FROM monhoc WHERE mamh = '" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                mh.setMamh(rs.getString("mamh"));
                mh.setTenmh(rs.getString("tenmh"));
                mh.setSotinchi(rs.getInt("sotinchi"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mh;
    }

    public ArrayList<MonHocModel> getListDetailMonHoc(String id) {
        ArrayList<MonHocModel> ListMh = new ArrayList<MonHocModel>();
        try {
            String sql = "SELECT * FROM monhoc WHERE mamh like '" + id + "%'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                MonHocModel mh = new MonHocModel();
                mh.setMamh(rs.getString("mamh"));
                mh.setTenmh(rs.getString("tenmh"));
                mh.setSotinchi(rs.getInt("sotinchi"));
                ListMh.add(mh);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ListMh;
    }
    
    @Override
    public boolean editMonHoc(String id, MonHocModel mh) {
        int count = 0;
        try {
            String sql = "UPDATE monhoc SET mamh ='" + mh.getMamh() + "', tenmh ='" + mh.getTenmh() 
                    + "', sotinchi =" + mh.getSotinchi() + " WHERE mamh ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean addMonHoc(MonHocModel mh) {
        int count = 0;
        try {
            String sql = "INSERT INTO monhoc(mamh, tenmh, sotinchi) VALUES('" + mh.getMamh() + "', '" + mh.getTenmh() 
                    + "', " + mh.getSotinchi() + ");";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public boolean deleteMonHoc(String id) {
        int count = 0;
        try {
            String sql = "DELETE FROM monhoc WHERE mamh ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }

    @Override
    public ArrayList<SinhVienModel> getAllSinhVien(String id) {
        ArrayList<SinhVienModel> list = new ArrayList<>();
        try {
            String sql = "SELECT sinhvien.* FROM sinhvien INNER JOIN sinhvien_monhoc ON sinhvien.masv = sinhvien_monhoc.masv"
                    + " WHERE sinhvien_monhoc.mamh ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                SinhVienModel sv = new SinhVienModel();
                sv.setMasv(rs.getInt("masv"));
                sv.setHoten(rs.getString("hoten"));
                sv.setDiachi(rs.getString("diachi"));
                sv.setKhoa(rs.getString("khoa"));
                sv.setLop(rs.getString("lop"));
                sv.setNgaysinh(rs.getString("ngaysinh").toString());
                list.add(sv);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<CaThiModel> getAllCaThiOfMonHoc(String id) {
        ArrayList<CaThiModel> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM cathi WHERE mamh ='" + id + "'";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                CaThiModel ct = new CaThiModel();
                ct.setMact(rs.getString("mact"));
                ct.setTenct(rs.getString("tenct"));
                ct.setMakt(rs.getString("makt"));
                ct.setMamh(rs.getString("mamh"));
                ct.setNgaythi(rs.getString("ngaythi"));
                ct.setBatdau(rs.getString("batdau"));
                ct.setKetthuc(rs.getString("ketthuc"));
                ct.setSoluongdk(rs.getInt("soluongdk"));
                list.add(ct);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;   
    }

    @Override
    public ArrayList<SinhVienModel> getAllCamThi(String id) {
        ArrayList<SinhVienModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT sinhvien.* FROM sinhvien INNER JOIN sinhvien_monhoc ON sinhvien.masv = sinhvien_monhoc.masv"
                    + " WHERE sinhvien_monhoc.trangthai = 0 AND sinhvien_monhoc.mamh ='" + id + "'";
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                SinhVienModel sv = new SinhVienModel();
                sv.setMasv(rs.getInt("masv"));
                sv.setHoten(rs.getString("hoten"));
                sv.setDiachi(rs.getString("diachi"));
                sv.setNgaysinh(rs.getDate("ngaysinh").toString());
                sv.setLop(rs.getString("lop"));
                sv.setKhoa(rs.getString("khoa"));
                list.add(sv);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<SinhVienModel> getAllDuDKThi(String id) {
        ArrayList<SinhVienModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT sinhvien.* FROM sinhvien INNER JOIN sinhvien_monhoc ON sinhvien.masv = sinhvien_monhoc.masv"
                    + " WHERE sinhvien_monhoc.trangthai = 1 AND sinhvien_monhoc.mamh ='" + id + "'";
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                SinhVienModel sv = new SinhVienModel();
                sv.setMasv(rs.getInt("masv"));
                sv.setHoten(rs.getString("hoten"));
                sv.setDiachi(rs.getString("diachi"));
                sv.setNgaysinh(rs.getDate("ngaysinh").toString());
                sv.setLop(rs.getString("lop"));
                sv.setKhoa(rs.getString("khoa"));
                list.add(sv);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    // sinh vien cam thi
    public boolean addSinhVienMonHoc(SinhVienMonHocModel mh) {
        int count = 0;
        try {
            String sql = "INSERT INTO sinhvien_monhoc(masv, mamh, trangthai) VALUES(" + mh.getMasv()+ ", '" + mh.getMamh()
                    + "', " + mh.getTrangthai()+ ");";
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            count = st.executeUpdate(sql);
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count != 0);
    }
    
    public ArrayList<SinhVienMonHocModel> getDS(int trangthai) {
        ArrayList<SinhVienMonHocModel> list = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM sinhvien_monhoc WHERE trangthai = " + trangthai;
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                SinhVienMonHocModel sv = new SinhVienMonHocModel();
                sv.setMasv(rs.getInt("masv"));
                sv.setMamh(rs.getString("mamh"));
                sv.setTrangthai(rs.getInt("trangthai"));
                list.add(sv);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SinhVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public SinhVienMonHocModel getDetailSVMH(int id) {
        SinhVienMonHocModel mh = new SinhVienMonHocModel();
        try {
            String sql = "SELECT * FROM sinhvien_monhoc WHERE masv = " + id ;
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                mh.setMasv(rs.getInt("masv"));
                mh.setMamh(rs.getString("mamh"));
                mh.setTrangthai(rs.getInt("trangthai"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mh;
    }
}
