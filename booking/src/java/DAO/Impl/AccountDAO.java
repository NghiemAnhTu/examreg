/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Impl;

import DAO.IAccountDAO;
import Model.TaiKhoanModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuNghiem
 */
public class AccountDAO implements IAccountDAO{
    
    public Connection getConnection() throws SQLException{
        String DB_URL = "jdbc:mysql://localhost:3306/examregdb";
        String USER_NAME = "root";
        String PASSWORD = "";
        Connection con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
        return con;
    }

    @Override
    public List<TaiKhoanModel> getAll() {
        List<TaiKhoanModel> list = new ArrayList<>();
        try {
            Connection con = getConnection();
            String sql = "SELECT * FORM taikhoan"; 
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {                
                TaiKhoanModel taikhoan = new TaiKhoanModel();
                taikhoan.setUsername(rs.getInt("username"));;
                taikhoan.setPassword(rs.getString("password"));
                list.add(taikhoan);
            }
            rs.close();
            st.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public TaiKhoanModel getAccount(int id) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        TaiKhoanModel taikhoan = new TaiKhoanModel();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM taikhoan WHERE username = " + id;
            ResultSet rs = st.executeQuery(sql); 
            while (rs.next()) {                
                taikhoan.setUsername(rs.getInt("username"));;
                taikhoan.setPassword(rs.getString("password"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return taikhoan;
    }

    @Override
    public boolean addAccount(TaiKhoanModel tkm) {
        int count = 0;
        try {
            Connection con = getConnection();
            Statement st = con.createStatement();
            String sql = "INSERT into taikhoan(username, password) values(" + tkm.getUsername() + ", '" + tkm.getPassword() + "')";
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false :  true; 
    }

    @Override
    public boolean editAccount(int id, TaiKhoanModel tk) {
        int count = 0;
        try {
            Connection con = getConnection();
            Statement st = con.createStatement();
            String sql = "UPDATE taikhoan SET username =" + tk.getUsername() + ", password = " + tk.getPassword()
                    + "WHERE username =" + id;
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false :  true; 
    }

    @Override
    public boolean deleteAccount(int tk) {
        int count = 0;
        try {
            Connection con = getConnection();
            Statement st = con.createStatement();
            String sql = "DELETE FROM taikhoan WHERE username =" + tk;
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false :  true; 
    }

    public boolean addDSAccount(String sql) {
        int count = 0;
        try {
            Connection con = getConnection();
            Statement st = con.createStatement();
            count = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? false :  true; 
    }
    
    public boolean checkTK(int masv){
        TaiKhoanModel tk = getAccount(masv);
        if(tk.getUsername() > 0){
            return false;
        }
        return true;
    }
    
}
