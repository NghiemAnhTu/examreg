/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.CaThiModel;
import Model.SinhVienCaThiModel;
import Model.SinhVienModel;
import java.util.ArrayList;

/**
 *
 * @author TuNghiem
 */
public interface ISinhVienCaThiDAO {
    // danh sach cac lich thi 1 sinh vien da dang ki
    ArrayList<CaThiModel> getAllCaThi(int id);
//    CaThiModel getCaThi(String mact);
    // them sua, xoa
    boolean addSinhVienCaThi(SinhVienCaThiModel sv);
    boolean editSinhVienCaThi(SinhVienCaThiModel old, SinhVienCaThiModel newO);
    boolean deleteSinhVienCaThi(SinhVienCaThiModel sv);
    
    // danh sach sinh vien dang ki 1 ca thi
    ArrayList<SinhVienModel> getAllSinhVienThi(String mact);
}
