/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.CaThiModel;
import Model.MonHocModel;
import Model.SinhVienModel;
import java.util.ArrayList;

/**
 *
 * @author TuNghiem
 */
public interface ISinhVienDAO {
    // thong tin sinh vien
    ArrayList<SinhVienModel> getAllSinhVien();
    SinhVienModel getSinhVien(int id);
    boolean editSinhVien(int id, SinhVienModel sv);
    boolean addSinhVien(SinhVienModel sv);
    boolean deleteSinhVien(int id);
    
    // thong tin cac mon hoc
    ArrayList<MonHocModel> getAllMonHocThi(int id);
    ArrayList<MonHocModel> getAllMonHocCamThi(int id);
    // danh sach cac lich thi da dang ki
    ArrayList<CaThiModel> getAllCaThi(int id);
    
    
    
    
    
}
