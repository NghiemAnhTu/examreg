/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.PhongThiModel;
import java.util.ArrayList;

/**
 *
 * @author TuNghiem
 */
public interface IPhongThiDAO {
    ArrayList<PhongThiModel> getAllPhongThi();
    PhongThiModel getPhongThi(String id);
    boolean editPhongThi(String id, PhongThiModel ptm);
    boolean deletePhongThi(String id);
    boolean addPhongThi(PhongThiModel pt);
    
}
