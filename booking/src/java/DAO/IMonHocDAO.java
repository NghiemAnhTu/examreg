/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.CaThiModel;
import Model.MonHocModel;
import Model.SinhVienModel;
import java.util.ArrayList;

/**
 *
 * @author TuNghiem
 */
public interface IMonHocDAO {
    // thong tin cac mon hoc
    ArrayList<MonHocModel> getAllMonHoc();
    MonHocModel getDetailMonHoc(String id);
    // thao tac voi mon hoc
    boolean editMonHoc(String id, MonHocModel mh);
    boolean addMonHoc(MonHocModel mh);
    boolean deleteMonHoc(String id);
    // danh sach sinh vien
    ArrayList<SinhVienModel> getAllSinhVien(String id);
    // danh sach cac ca thi
    ArrayList<CaThiModel> getAllCaThiOfMonHoc(String id);
    // danh sach sinh vien bi cam thi
    ArrayList<SinhVienModel> getAllCamThi(String id);
    // danh sach du dieu kien thi
    ArrayList<SinhVienModel> getAllDuDKThi(String id);
}
