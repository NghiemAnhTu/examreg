/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.LichThiModel;
import java.util.ArrayList;

/**
 *
 * @author TuNghiem
 */
public interface ILichThiDAO {
    ArrayList<LichThiModel> getAllLichThi();
    LichThiModel getLichThi(String id);
    //
    boolean addLichThi(LichThiModel lt);
    boolean editLichThi(String id, LichThiModel lt);
    boolean deleteLichThi(String id);
}
