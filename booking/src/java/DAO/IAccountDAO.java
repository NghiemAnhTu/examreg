/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.TaiKhoanModel;
import java.util.List;

/**
 *
 * @author TuNghiem
 */
public interface IAccountDAO {
    List<TaiKhoanModel> getAll();
    TaiKhoanModel getAccount(int id);
    boolean addAccount(TaiKhoanModel tkm);
    boolean editAccount(int id, TaiKhoanModel tk);
    boolean deleteAccount(int tk);
            
    
}
