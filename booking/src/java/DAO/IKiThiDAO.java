/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.KiThiModel;
import java.util.ArrayList;

/**
 *
 * @author TuNghiem
 */
public interface IKiThiDAO {
    ArrayList<KiThiModel> getAllKiThi();
    KiThiModel getKiThi(String id);
    //
    boolean editKiThi(String id, KiThiModel kt);
    boolean addKiThi(KiThiModel kt);
    boolean deleteKiThi(String id);
}
