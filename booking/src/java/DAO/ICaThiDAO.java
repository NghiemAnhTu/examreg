/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.CaThiModel;
import Model.MonHocModel;
import Model.PhongThiModel;
import Model.SinhVienModel;
import java.util.ArrayList;

/**
 *
 * @author TuNghiem
 */
public interface ICaThiDAO {
    ArrayList<CaThiModel> getAllCaThi();
    CaThiModel getCaThi(String id);
    
    boolean addCaThi(CaThiModel ct);
    boolean editCaThi(String id, CaThiModel ct);
    boolean deleteCaThi(String id);
    
    // danh sach cac sinh vien trong 1 phong thi vao 1 ca thi
    ArrayList<SinhVienModel> getAllSinhVienInPhongThiOfCaThi( String idct);
    // danh sach cac mon hoc trong 1 ca thi
    ArrayList<MonHocModel> getAllMonHocOfNgayThi(String idCathi);
}
