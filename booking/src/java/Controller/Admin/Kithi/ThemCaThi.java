/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.Kithi;

import DAO.Impl.CaThiDAO;
import Model.CaThiModel;
import Model.CaThiPhongThiModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet("/themcathi")
public class ThemCaThi extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("taocathi.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CaThiDAO ctd = new CaThiDAO();
        String mapt = req.getParameter("mapt");
        String mact = req.getParameter("mact");
        String tenct = req.getParameter("tenct");
        String mamh = req.getParameter("mamh");
        String ngaythi = req.getParameter("ngaythi");
        String batdau = req.getParameter("thoigianbatdau");
        String ketthuc = req.getParameter("thoigianketthuc");
        String soluongdk = req.getParameter("soluongdk");
        String makt = req.getParameter("makt");
        if(!mapt.equals("None") && mact.length() != 0 && tenct.length() != 0 && !mamh.equals("None") && ngaythi.length() != 0 && batdau.length() != 0 && ketthuc.length() != 0 && !makt.equals("None") && soluongdk.length() != 0){
            CaThiModel ctm = new CaThiModel();
            CaThiPhongThiModel ctptm = new CaThiPhongThiModel();
            ctptm.setMact(mact);
            ctptm.setMapt(mapt);
            ctm.setMact(mact);
            ctm.setTenct(tenct);
            ctm.setMakt(makt);
            ctm.setMamh(mamh);
            ctm.setNgaythi(ngaythi);
            ctm.setBatdau(batdau);
            ctm.setKetthuc(ketthuc);
            ctm.setSoluongdk(Integer.parseInt(soluongdk));
            
            System.out.println(makt + "|" + mamh);
            if(ctd.addCaThi(ctm) && ctd.addCaThiPhongThi(ctptm)){
                resp.sendRedirect("DanhSachCaThi");
            }else{
                resp.sendRedirect("themcathi");
            }
        }else{
            resp.sendRedirect("themcathi");
        }
        
    }
    
    
    
}
