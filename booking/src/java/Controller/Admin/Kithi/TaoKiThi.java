/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.Kithi;

import DAO.Impl.KiThiDAO;
import Model.KiThiModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet("/taokithi")
public class TaoKiThi extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("taokithi.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String makt = req.getParameter("makt");
        String tenkt = req.getParameter("tenkt");
        
        if(makt.length() != 0 && tenkt.length() != 0){
            
            KiThiDAO ktd = new KiThiDAO();
            KiThiModel ktm = new KiThiModel();
            ktm.setMakt(makt);
            ktm.setTenkt(tenkt);
            if(ktd.addKiThi(ktm)){
                
                resp.sendRedirect("DanhSachKiThi");
            }else{
                resp.sendRedirect("taokithi.jsp");
            }
        }else{
            resp.sendRedirect("taokithi.jsp");
        }
    }
    
    
    
}
