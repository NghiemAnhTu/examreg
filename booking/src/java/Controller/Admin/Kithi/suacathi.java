/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.Kithi;

import DAO.Impl.CaThiDAO;
import Model.CaThiModel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet("/suacathi")
public class suacathi extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String emact = req.getParameter("emact");
            String mact = req.getParameter("mact");
            String tenct = req.getParameter("tenct");
            String mamh = req.getParameter("mamh");
            String ngaythi = req.getParameter("ngaythi");
            String batdau = req.getParameter("thoigianbatdau");
            String ketthuc = req.getParameter("thoigianketthuc");
            String soluongdk = req.getParameter("soluongdk");
            String makt = req.getParameter("makt");
            if(mact.length() != 0 && tenct.length() != 0 && !mamh.equals("None") && ngaythi.length() != 0 && batdau.length() != 0 && ketthuc.length() != 0 && !makt.equals("None") && soluongdk.length() != 0){
                CaThiDAO ctd = new CaThiDAO();
                CaThiModel ctm = new CaThiModel();
                ctm.setMact(mact);
                ctm.setTenct(tenct);
                ctm.setMakt(makt);
                ctm.setMamh(mamh);
                ctm.setNgaythi(ngaythi);
                ctm.setBatdau(batdau);
                ctm.setKetthuc(ketthuc);
                ctm.setSoluongdk(Integer.parseInt(soluongdk));
                if (ctd.editCaThi(emact, ctm)){
                    resp.sendRedirect("DanhSachCaThi");
                }else{
                    resp.sendRedirect("suacathi");
                }
            }else{
                resp.sendRedirect("suacathi");
            }
            
    }
    
}
