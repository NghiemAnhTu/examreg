/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.Kithi;

import DAO.Impl.PhongThiDAO;
import Model.PhongThiModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet("/TaoPhongThi")
public class ThemPhongThi extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("taophongthi.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String mapt = req.getParameter("mapt");
        String tenpt = req.getParameter("tenpt");
        String diachi = req.getParameter("diachi");
        String somay = req.getParameter("somay");
        System.out.println(mapt + "|" + tenpt + "|" + diachi + "|" + somay);
        if(mapt.length() != 0 && tenpt.length() != 0 && diachi.length() != 0 && somay != ""){
            PhongThiDAO ptd = new PhongThiDAO();
            PhongThiModel ptm = new PhongThiModel();
            ptm.setMapt(mapt);
            ptm.setDiachi(diachi);
            ptm.setSomay(Integer.parseInt(somay));
            ptm.setTenpt(tenpt);
            if(ptd.addPhongThi(ptm)){
                resp.sendRedirect("DanhSachPhongThi");
            }else{
                resp.sendRedirect("TaoPhongThi");
            }
        }else{
            resp.sendRedirect("TaoPhongThi");
        }
    }
    
    
    
}
