/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.Account;

import DAO.Impl.AccountDAO;
import DAO.Impl.SinhVienDAO;
import Model.SinhVienModel;
import Model.TaiKhoanModel;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */

@WebServlet(value = "/themsinhvien")
public class ThemSinhVien extends HttpServlet{
    SinhVienDAO svd = new SinhVienDAO();
    

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("themsinhvien.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String masv = req.getParameter("masv");
        String hoten = req.getParameter("hoten");
        String diachi = req.getParameter("diachi");
        String lop = req.getParameter("lop");
        String khoa = req.getParameter("khoa");
        String ns = req.getParameter("ngaysinh");
        TaiKhoanModel tkm = new TaiKhoanModel();
        tkm.setUsername(Integer.parseInt(masv));
        tkm.setPassword("12345678");
        AccountDAO acd = new AccountDAO();
        SinhVienDAO svd = new SinhVienDAO();
        SinhVienModel svm = new SinhVienModel();
        svm.setDiachi(diachi);
        svm.setHoten(hoten);
        svm.setKhoa(khoa);
        svm.setLop(lop);
        svm.setMasv(Integer.parseInt(masv));
        svm.setNgaysinh(ns);
        System.out.println(svm.getHoten());
        
        if(acd.checkTK(Integer.parseInt(masv))){
            acd.addAccount(tkm);
            svd.addSinhVien(svm);
            
            resp.sendRedirect("DanhSachSinhVien");
//            RequestDispatcher rd = req.getRequestDispatcher("DanhSachSinhVien");
//            rd.forward(req, resp);
        }else{
            resp.sendRedirect("themsinhvien");
        }
        
    }
    
    
}
