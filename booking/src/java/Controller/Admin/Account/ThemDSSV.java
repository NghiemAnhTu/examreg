/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.Account;

import DAO.Impl.AccountDAO;
import DAO.Impl.SinhVienDAO;
import Model.SinhVienModel;
import Model.TaiKhoanModel;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet("/themdssv")
public class ThemDSSV extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("themdanhsachsinhvien.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AccountDAO acd = new AccountDAO();
        SinhVienDAO svd = new SinhVienDAO();
        String s = req.getParameter("filedanhsachsv");
        String fileName = "C:\\Users\\TuNghiem\\Desktop\\BTL\\web\\booking\\src\\java\\Data\\" + s;
        ArrayList<String> list = new ArrayList<>();
        ArrayList<SinhVienModel> listSV = new ArrayList<>();
        // doc file
        try(Stream<String> stream = Files.lines(Paths.get(fileName),StandardCharsets.UTF_8)){//đưa về dạng chuẩn utf8
            stream.forEach((String line) ->{
                list.add(line);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        String sql = "";
        String sql1 = "";
        for(int i = 1; i < list.size(); i++){
            System.out.println(list.get(i));
            String tmp[] = list.get(i).split(",");
            TaiKhoanModel tkm = new TaiKhoanModel();
            SinhVienModel svm = new SinhVienModel();
            //
            tkm.setUsername(Integer.parseInt(tmp[0]));
            tkm.setPassword(tmp[1]);
            acd.addAccount(tkm);
            //
            svm.setMasv(Integer.parseInt(tmp[0]));
            svm.setHoten(tmp[2]);
            svm.setNgaysinh(tmp[3]);
            svm.setLop(tmp[4]);
            svm.setKhoa(tmp[5]);
            svm.setDiachi(tmp[6]);
           
            
            svd.addSinhVien(svm);
            
        }
//        System.out.println(sql + "\n" + sql1);
        //acd.addDSAccount(sql1);
//        svd.addDSSinhVien(sql);
//        req.setAttribute("file", s);
//        req.setAttribute("DSSV", listSV);
        RequestDispatcher rd = req.getRequestDispatcher("themdanhsachsinhvien.jsp");
        rd.forward(req, resp);
    }
    
    
    
}
