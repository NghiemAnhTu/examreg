/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.Account;

import DAO.Impl.SinhVienDAO;
import Model.SinhVienModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet("/suasinhvien")
public class SuaThongTinhSinhVien extends HttpServlet{
    SinhVienDAO svd = new SinhVienDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        int emasv = Integer.parseInt(req.getParameter("emasv"));
        String masv = req.getParameter("masv");
        String hoten = req.getParameter("hoten");
        String diachi = req.getParameter("diachi");
        String lop = req.getParameter("lop");
        String khoa = req.getParameter("khoa");
        String ns = req.getParameter("ngaysinh");
        System.out.println(emasv);
        if(masv.length() != 0 && hoten.length() != 0 && diachi.length() != 0 && lop.length() != 0 && khoa.length() != 0 && ns.length() != 0){
            System.out.println(emasv);
            SinhVienModel svm = new SinhVienModel();
            svm.setDiachi(diachi);
            svm.setHoten(hoten);
            svm.setKhoa(khoa);
            svm.setLop(lop);
            svm.setMasv(Integer.parseInt(masv));
            svm.setNgaysinh(ns);
            
            if(svd.editSinhVien(emasv,svm)){
                System.out.println(emasv);
                resp.sendRedirect("DanhSachSinhVien");
            }else{
                System.out.println(emasv);
                RequestDispatcher rd = req.getRequestDispatcher("suasinhvien.jsp");
                rd.forward(req, resp);
            }
        }else{
            RequestDispatcher rd = req.getRequestDispatcher("suasinhvien.jsp");
            rd.forward(req, resp);
        }
    }
    
}
