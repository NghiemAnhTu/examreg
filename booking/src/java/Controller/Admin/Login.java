/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin;

import DAO.Impl.AccountDAO;
import Model.TaiKhoanModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author TuNghiem
 */
@WebServlet( urlPatterns = {"/login", "/trangchu"})
public class Login extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("/login.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AccountDAO ad = new AccountDAO();
        String user = req.getParameter("username");
        String pass = req.getParameter("password");
        TaiKhoanModel tkm = ad.getAccount(Integer.parseInt(user));
        if(tkm.getUsername() != 0 && tkm.getPassword().equals(pass)){
            if(tkm.getUsername() == 100){
                resp.sendRedirect("admin-home.jsp");
            }else{
                HttpSession session = req.getSession();
                session.isNew();
                session.setAttribute("ID", user);
                System.out.println(user);
                resp.sendRedirect("user-home.jsp");
//                RequestDispatcher rd = req.getRequestDispatcher("user-home.jsp");
//                rd.forward(req, resp);
                
            }
        }else{
            RequestDispatcher rd = req.getRequestDispatcher("/login");
            rd.forward(req, resp);
        }
    }
    
    
    
}
