/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.ExamReg;

import DAO.Impl.MonHocDAO;
import Model.SinhVienMonHocModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet(name = "DanhSachCamThi", urlPatterns = {"/DanhSachCamThi"})
public class DanhSachCamThi extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String s = request.getParameter("filecamthi");
            String fileName = "C:\\Users\\TuNghiem\\Desktop\\BTL\\web\\booking\\src\\java\\Data\\" + s;
            ArrayList<String> list = new ArrayList<>();
            System.out.println(fileName);
            ArrayList<SinhVienMonHocModel> listSV = new ArrayList<>();
            // doc file
            try(Stream<String> stream = Files.lines(Paths.get(fileName),StandardCharsets.UTF_8)){//đưa về dạng chuẩn utf8
                stream.forEach((String line) ->{
                    list.add(line);
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(s != null){
                String tmp[] = list.get(8).split(",");
                String mamh = tmp[2];
                System.out.println(mamh);
                MonHocDAO mhd = new MonHocDAO();
                for(int i = 11; i < list.size(); i++){
                    SinhVienMonHocModel svmhm = new SinhVienMonHocModel();
                    String tm[] = list.get(i).split(",");
                    String masv = tm[1];
                    String tensv = tm[2];
                    svmhm.setMasv(Integer.parseInt(masv));
                    svmhm.setMamh(mamh);
                    svmhm.setTrangthai(0);
                    mhd.addSinhVienMonHoc(svmhm);
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher("danhsachcamthi.jsp");
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
