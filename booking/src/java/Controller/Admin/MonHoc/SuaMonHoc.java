/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.MonHoc;

import DAO.Impl.MonHocDAO;
import Model.MonHocModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet("/suamonhoc")
public class SuaMonHoc extends HttpServlet{
    MonHocDAO mhd = new MonHocDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String mamhc = req.getParameter("suamamh");
        String mamh = req.getParameter("inpmamh");
        String tenmh = req.getParameter("inptenmh");
        String sotinchi = req.getParameter("inpsotinchi");
        
//        System.out.println(id);
        if(mamh.length() != 0 && tenmh.length() != 0 && sotinchi.length() != 0 ){
            MonHocModel mhm = new MonHocModel();
            mhm.setMamh(mamh);
            mhm.setSotinchi(Integer.parseInt(sotinchi));
            mhm.setTenmh(tenmh);
            if(mhd.editMonHoc(mamhc, mhm)){
                // them thanh cong
                resp.sendRedirect("TimKiemMaMh");
            }else{
                RequestDispatcher rd = req.getRequestDispatcher("suamonhoc.jsp");
                rd.forward(req, resp);
            }
        }else{
            RequestDispatcher rd = req.getRequestDispatcher("suamonhoc.jsp");
            rd.forward(req, resp);
            // thong bao
        }
        
    }
    
}
