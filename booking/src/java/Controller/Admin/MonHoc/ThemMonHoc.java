/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Admin.MonHoc;

import DAO.Impl.MonHocDAO;
import Model.MonHocModel;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TuNghiem
 */
@WebServlet(value = "/themmonhoc")
public class ThemMonHoc extends HttpServlet{
    MonHocDAO mhd = new MonHocDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        RequestDispatcher rd = req.getRequestDispatcher("themmonhoc.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String mamh = req.getParameter("mamh");
        String tenmh = req.getParameter("tenmh");
        String sotinchi = req.getParameter("sotinchi");
        MonHocModel mhm = new MonHocModel();
        mhm.setMamh(mamh);
        mhm.setTenmh(tenmh);
        mhm.setSotinchi(Integer.parseInt(sotinchi));
        if(mhd.addMonHoc(mhm)){
//            PrintWriter out = resp.getWriter();
//            out.println("<script>alert('Thêm mon học thanh cong')</script>");
//            out.println(" mon học thanh cong");
            resp.sendRedirect("TimKiemMaMh");
//            RequestDispatcher rd = req.getRequestDispatcher("themmonhoc.jsp");
//            rd.forward(req, resp);
            
        }else{
            System.out.println("Thêm thất bại");
            RequestDispatcher rd = req.getRequestDispatcher("themmonhoc.jsp");
            rd.forward(req, resp);
        }
    }
    
    
    
    
}
